!#/bin/sh
api=api
phash=f9c4d6e92042d1dc88a44122d29a53652741cb846bf97f2d58b4982134d1d817
python crestcli.py create --host localhost --type tags --tag MYTAG-02 --params timeType=time --port 8090 --api ${api}
python crestcli.py create --host localhost --type payloads --since 5100000 --tag MYTAG-02 --inpfile ./data/test-1.json --port 8090 --api ${api}
python crestcli.py create --host localhost --type payloads --since 11100000 --tag MYTAG-02 --inpfile ./data/test-2.json --port 8090 --api ${api}
python crestcli.py --host localhost ls --type iovs -t MYTAG-02 --port 8090 --api ${api}
python crestcli.py --host localhost get --type payloads -p ${phash} --port 8090 --api ${api}
