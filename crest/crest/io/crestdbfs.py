"""
 HTTP client tool to exchange with VHF-PKT-DB

 Retries requests with power law delays and a max tries limit

 @author: henri.louvin@cea.fr
"""

# Log
import logging
# JSON
import json
import datetime
from ..utils.crestutils import *

log = logging.getLogger('crestdb_fs')


class CrestDbFs:
    """
    (A)synchronous FS client
    """

    def __init__(self, server_url=None, max_tries=1,  # pylint: disable=R0913
                 backoff_factor=1, asynchronous=False, loop=None, token=None):
        if server_url is None:
            server_url = '/tmp/crest/fs'
        self.basedir_url = server_url
        create_dir(path=self.basedir_url)
        self.endpoints = {'tags': '/tags', 'iovs': '/iovs',
                          'payloads': '/payloads', 'globaltags': '/globaltags',
                          'maps': '/globaltagmaps'}

    def search_tags(self, page=0, size=100, sort='name:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_tags(name='SVOM', payloadspec=JSON)
        ?by=name:SVOM,payloadspec:JSON”
        """
        # define output fields
        valid_filters = ['name', 'payloadspec', 'timetype']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)

        # prepare request arguments
        by_crit = create_search(cdic=kwargs)
        criteria = {'by': by_crit}
        log.debug(f'Use criteria {by_crit}')
        criteria['page'] = page
        criteria['size'] = size
        criteria['sort'] = sort
        loc_headers = self.build_header()
        # send request
        resp = self.get(self.endpoints['tags'], params=criteria, headers=loc_headers)
        return resp.json()

    def search_maps(self, name=None, mode='Trace'):
        """
        request and export data from the database in json format
        usage example: search_maps(name='A-GT-OR-T-NAME')
        mode: trace|backtrace
        name: if mode is trace, then this represents a GT name, otherwise is a Tag name
        """
        # prepare headers
        map_header = {'X-Crest-MapMode': mode}
        # send request
        loc_headers = self.build_header(hdr=map_header)

        resp = self.get(self.endpoints['maps'] + f'/{name}', headers=loc_headers)
        return resp.json()

    def search_globaltags(self, page=0, size=100, sort='name:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_globaltags(name='SVOM')
        """
        # define output fields
        valid_filters = ['name', 'scenario', 'release', 'workflow']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)

        # prepare request arguments
        by_crit = create_search(cdic=kwargs)
        criteria = {'by': by_crit}
        log.debug(f'Use criteria {by_crit}')
        criteria['page'] = page
        criteria['size'] = size
        criteria['sort'] = sort
        loc_headers = self.build_header()

        # send request
        resp = self.get(self.endpoints['globaltags'], params=criteria, headers=loc_headers)
        return resp.json()

    def create_tags(self, name=None, mode='create', **kwargs):
        """
        import data into the database in json format
        usage example: create_tags(name='SVOM-01', payloadSpec='JSON',
        timeType='time',description='a tag',synchronization='none')
        """
        # define output fields
        valid_fields = ['payloadSpec', 'timeType', 'description', 'synchronization', 'endOfValidity']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = {
            'name': name,
            'payloadSpec': 'JSON',
            'timeType': 'time',
            'description': 'a new tag',
            'synchronization': 'none',
            'lastValidatedTime': 0,
            'endOfValidity': 0,
            'insertionTime': int(datetime.datetime.utcnow().timestamp()),
            'modificationTime': int(datetime.datetime.utcnow().timestamp())}
        for key, val in kwargs.items():
            body_req[key] = val
        log.info('Create tag : %s', json.dumps(body_req))
        # send request
        resp = None
        setdto = {}
        tag_arr = []
        tag_file = self.basedir_url + self.endpoints['tags'] + '/tags.json'
        tag_exists = False

        setdto = read_set(tag_file)
        updiv = None
        remiv = None
        if setdto is None:
            tag_arr = [body_req]
            setdto = create_set(tag_arr, 'tags', 'TagSetDto')
        else:
            tag_arr = setdto['resources']
            for iv in tag_arr:
                if iv['name'] == body_req['name']:
                    tag_exists = True
                    if 'update' == mode:
                        log.info(f'Update existing tag {iv}')
                        updiv = iv
                        remiv = iv
                        updiv['timeType'] = body_req['timeType']
                        updiv['description'] = body_req['description']
                        updiv['synchronization'] = body_req['synchronization']
                        updiv['modificationTime'] = int(datetime.datetime.utcnow().timestamp())
            if not tag_exists:
                tag_arr.append(body_req)
                setdto['resources'] = tag_arr
            if 'update' == mode and tag_exists:
                log.info(f'Update element {updiv} and remove {remiv}')
                tag_arr.remove(remiv)
                tag_arr.append(updiv)
                setdto['resources'] = tag_arr

        write_set(tag_file, setdto)
        return body_req

    def create_maps(self, tag=None, globaltag=None, record=None, label=None):
        """
        import data into the database in json format
        usage example: create_maps(tag='MXT-CONFIG-01', globaltag='SVOM-01',
        record='ok',label='mxt-config')
        """
        # prepare request arguments
        body_req = {
            'globalTagName': globaltag,
            'record': record,
            'label': label,
            'tagName': tag
        }
        log.info('Create mapping : %s', json.dumps(body_req))
        loc_headers = self.build_header()

        # send request
        resp = self.post(self.endpoints['maps'], json=body_req, headers=loc_headers)
        return resp.json()

    def create_iovs(self, name=None, since=None, phash=None, **kwargs):
        """
        import data into the database in json format
        usage example: create_iovs(name='SVOM-01', phash='somehash',
        since=1000)
        """
        # define output fields
        valid_fields = ['insertionTime']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = {
            'tagName': name,
            'since': since,
            'payloadHash': phash,
            'insertionTime': None}

        for key, val in kwargs.items():
            body_req[key] = val
        log.info('Create iov if the hash is known : %s', json.dumps(body_req))
        # send request
        iov_file = self.basedir_url + self.endpoints['tags'] + '/' + name + '/iovs.json'
        iov_exists = False
        setdto = read_set(iov_file)
        if setdto is None:
            iov_arr = [body_req]
            setdto = create_set(iov_arr, 'iovs', 'IovSetDto')
        else:
            iov_arr = setdto['resources']
            for iv in iov_arr:
                if iv['since'] == body_req['since'] and iv['payloadHash'] == body_req['payloadHash']:
                    iov_exists = True
            if not iov_exists:
                iov_arr.append(body_req)
                setdto['resources'] = iov_arr

        write_set(iov_file, setdto)
        return setdto

    def create_globaltags(self, name=None, mode='create', **kwargs):
        """
        import data into the database in json format
        usage example: create_globaltags(name='GT-SVOM-01', validity=0,
        description='some gt',release='a release',scenario='none',workflow='onl',
        type='T',snapshotTime='2020-01-01T10:00:00')
        """
        # define output fields
        valid_fields = ['validity', 'description', 'type', 'release', \
                        'scenario', 'workflow', 'snapshotTime']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = {
            'name': name,
            'release': 'none',
            'type': 'T',
            'description': 'a new gtag',
            'scenario': 'none',
            'validity': 0,
            'workflow': 'all',
            'snapshotTime': None,
            'insertionTime': None}
        for key, val in kwargs.items():
            body_req[key] = val
        log.info('Create global tag : %s', json.dumps(body_req))
        setdto = {}
        gtag_arr = []
        gtag_file = self.basedir_url + self.endpoints['globaltags'] + '/globaltags.json'
        gtag_exists = False

        setdto = read_set(gtag_file)
        if setdto is None:
            gtag_arr = [body_req]
            setdto = create_set(gtag_arr, 'globaltags', 'GlobaltagSetDto')
        else:
            gtag_arr = setdto['resources']
            for iv in gtag_arr:
                if iv['name'] == body_req['name']:
                    gtag_exists = True
                    if 'update' == mode:
                        iv['release'] = body_req['release']
                        iv['type'] = body_req['type']
                        iv['description'] = body_req['description']
                        iv['scenario'] = body_req['scenario']
                        iv['workflow'] = body_req['workflow']
            if not gtag_exists:
                gtag_arr.append(body_req)
                setdto['resources'] = gtag_arr

        write_set(gtag_file, setdto)
        return body_req

    def create_payload(self, dto={}, **kwargs):
        """
        import data into the database.
        If you want to use a date as a string put timeformat = 'str' .
        usage example: create_payload(file='/tmp/temp-01.txt', dto={})
        """
        body_req = {
            'hash': dto['hash'],
            'setdto': dto
        }

        # prepare request arguments
        p_dir = write_payload(setdto=body_req, basedir= self.basedir_url + self.endpoints['payloads'])
        return p_dir

    def get_payload(self, phash=None, fout='/tmp/out.blob', **kwargs):
        """
        retrieve data from the database
        usage example: get_payload(phash=  ,fout='/tmp/out.blob')
        """
        # define output fields
        valid_fields = ['info']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        log.info('Get payload : %s', phash)
        # send request
        read_payload(phash, self.basedir_url + self.endpoints['payloads'])
        return fout
