from crest.utils import *
from crest.io import *
import re
import sys, os
import readline
import logging
import atexit
import argparse
from crest.utils import *
import json

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

# handler = logging.StreamHandler()
# format = "%(levelname)s:%(name)s: %(message)s"
# handler.setFormatter(logging.Formatter(format))
# log.addHandler(handler)

def socks():
    SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Crest browser.', add_help=False)
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-l", "--log", default='log.txt',
                        help="the log file name cleaned up, with info only on folders and tags")
    parser.add_argument("-o", "--output", default='out.json',
                        help="the output file name, a json dump of the dictionary")
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        socks()

    inp_file = args.log
    out_file = args.output

    # Using readlines()
    f1 = open(inp_file, 'r')
    log_lines = f1.readlines()

    count = 0
    # Strips the newline character
    cool_folders = {'globaltag': 'none', 'db': 'none', 'folders': {}}

    for line in log_lines:
        count += 1
        log.debug("Line{}: {}".format(count, line.strip()))
        if 'conditionsTag' in line:
            words = re.split(r'\s+', line.strip())
            globaltag = words[len(words) - 1].strip('\'').strip('.')
            log.info(f'Found conditions tag {globaltag} from words {words}')
            cool_folders['globaltag'] = globaltag
        if 'INFO Added taginfo' in line:
            words = re.split(r'\s+', line.strip())
            folder = words[len(words) - 1]
            log.info(f'Found folder {folder} from words {words}')
            cool_folders['folders'][folder] = {'tag': 'none'}
        if 'Setting up conditions DB access' in line:
            words = re.split(r'\s+', line.strip())
            dbname = words[len(words) - 1]
            cool_folders['db'] = dbname
        if ' HVS ' in line:
            words = re.split(r'\s+', line.strip())
            folder = words[len(words) - 1]
            tag = words[len(words) - 4]
            log.info(f'Found tag {tag} for folder {folder} in line {line}')
            cool_folders['folders'][folder]['tag'] = tag

    cool_http = HttpIo('http://atlasfrontier08.cern.ch:8000/coolrapi')
    criteria = {
        'db': cool_folders['db'],
        'schema': 'ATLAS_COOL%'
    }
    log.info('Access COOL information for DB %s' % cool_folders['db'])
    node_resp = cool_http.get('/nodes', params=criteria)
    cool_nodes = node_resp.json()
    crest_http = HttpIo('http://crest-01.cern.ch:8080/crestapi')

    for node in cool_nodes:
        node_fullpath = node['nodeFullpath']
        if node_fullpath in cool_folders['folders'].keys():
            cool_folders['folders'][node_fullpath]['schema'] = node['schemaName']
            cool_folders['folders'][node_fullpath]['type'] = node['nodeIovBase']
            cool_folders['folders'][node_fullpath]['versioning'] = node['folderVersioning']
            tag = cool_folders['folders'][node_fullpath]['tag']
            if tag != 'none':
                search_tag = f'name:{tag}'
                criteria = { 'by': search_tag}
                crest_tag_resp = crest_http.get('/tags', params=criteria)
                crest_tags = crest_tag_resp.json()
                if len(crest_tags['resources']) > 0:
                    cool_folders['folders'][node_fullpath]['crest'] = 'ok'
                else:
                    cool_folders['folders'][node_fullpath]['crest'] = 'missing'

                log.info(f'Found tag in crest {crest_tags}')

    f2 = open(out_file, 'w')
    json.dump(cool_folders, f2)
