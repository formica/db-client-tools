from crest.utils import *
from crest.io import *
import pandas as pd
import json
import matplotlib.pyplot as plt

import logging

logging.basicConfig(level=logging.INFO)


def create_search(cdic={}, by=''):
    # prepare request arguments
    # assume that the values in the dictionary are arrays
    by_crit = by
    for key, lval in cdic.items():
        for val in lval:
            a = val
            if val[0] not in ['>', '<', ':']:
                a = ':' + val
            by_crit += (f',{key}{a}')
    if len(by) == 0:
        return by_crit[1:]
    return by_crit


def socks():
    SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))


def check_tag_size(tagname=''):
    # lar_tiovs_set = cli.search_iovs(page=0, size=1000, sort='id.since:ASC', tagname=tn)
    tiovs_set = cli.select(cmd='iovpayloads', tagname=tagname)
    resources = tiovs_set['resources']
    for iov in resources:
        since = iov['since']
        hash = iov['payloadHash']
        size = iov['size']
        print(f'Tag {tagname} - Iov [ {since} ] => {hash} / {size}')
    return resources

def check_tag_df(tagname='', iovtype='time'):
    # lar_tiovs_set = cli.search_iovs(page=0, size=1000, sort='id.since:ASC', tagname=tn)
    tiovs_set = cli.select(cmd='iovpayloads', tagname=tagname)
    resources = tiovs_set['resources']
    jresources = json.dumps(resources)
    df = pd.read_json(jresources)
    if 'time' in iovtype:
        df['sinced'] = pd.to_datetime(df['since']/1000000000, unit='s')
    if 'run' in iovtype:
        df['sinced'] = (df['since']/2**32)
    df['iovdiff'] = df.sinced.diff()
    df['iovtype'] = iovtype
    return df

hostname = 'http://crest-01.cern.ch:8080/crestapi'

socks()
cli = CrestDbIo(server_url=hostname)
args_dic = {'name': ['LAR']}
lar_tags_set = cli.search_tags(page=0, size=1000, sort='name:ASC', **args_dic)
resources = lar_tags_set['resources']
for t in resources:
    tn = t['name']
    type = t['timeType']
    print(f'Found tag {tn} in crest.')
    df = check_tag_df(tn, t['timeType'])
    minsince = df['sinced'].min()
    maxsince = df['sinced'].max()
    maxdiff = df['iovdiff'].max()
    mindiff = df['iovdiff'].min()
    maxsize = df['size'].max()
    minsize = df['size'].min()

    g1 = df.groupby(['payloadHash']).size().reset_index(name='count')
    g1['ndred'] = g1['count'] - 1
    ndup = g1['ndred'].sum()
    nrows = df['since'].size
    print(f'Summary Tag {tn} [{type}] : niovs={nrows}, min(since)={minsince}, max(since)={maxsince},'
          f' mindiff={mindiff}, maxdiff={maxdiff}, min(size)={minsize}, max(size)={maxsize}, nduphash={ndup}')
#tn = 'LARTimeCorrectionOflCellTimeOffset-RUN2-UPD4-01'
