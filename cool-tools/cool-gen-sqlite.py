from PyCool import cool
import argparse
import sys

class CoolTools(object):

    def __init__(self, locconnstr=None, foldername=None, tagname=None, iov_since=0, iov_until=None):  # noqa: E501
        self._locconnstr = locconnstr
        self._foldername = foldername
        self._tagname = tagname
        self._iov_since = int(iov_since)
        if iov_until is None:
            self._iov_until = cool.ValidityKeyMax
        else:
            self._iov_until = int(iov_until)
        print(f'Using time range {self._iov_since} - {self._iov_until}')
        self._connection = None
        self._dbSvc = None
        print('Initialised cool sqlite connection')

    def connect(self, mode):
        try:
            self._dbSvc = cool.DatabaseSvcFactory.databaseService()
            if 'create' in mode:
                self._connection = self._dbSvc.createDatabase(self._locconnstr)
            else:
                self._connection = self._dbSvc.openDatabase(self._locconnstr, False)
            nodelist = self._connection.listAllNodes()
            for anode in nodelist:
                print(f'Found node {anode} in db')
        except Exception as e:
            print(e)

    def getFolder(self, folder):
        return self._connection.getFolder(folder)

    def getObjects(self, folder, since, until):
        cool_folder = self._connection.getFolder(folder)
        objs = cool_folder.browseObjects(since, until, cool.ChannelSelection(0))

    def copyFolder(self):
        connstr = "oracle://ATONR_ADG;schema=ATLAS_COOLOFL_MUONALIGN;dbname=CONDBR2;user=ATLAS_COOL_READER;password=COOLRED4PRO"
        remote_db = self._dbSvc.openDatabase(connstr)
        remote_folder = remote_db.getFolder(self._foldername);
        ## folder
        spec = remote_folder.folderSpecification()
        desc = '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'
        mode = cool.PayloadMode.SEPARATEPAYLOAD
        loc_folder = self._connection.createFolder(self._foldername, spec, desc, True)
        print(f'Created locally folder {loc_folder} from remote')
        remote_db.closeDatabase()
        return loc_folder

    def copyMuonAlignFolders(self):
        connstr = "oracle://ATONR_ADG;schema=ATLAS_COOLOFL_MUONALIGN;dbname=CONDBR2;user=ATLAS_COOL_READER;password=COOLRED4PRO"
        remote_db = self._dbSvc.openDatabase(connstr)
        desc = '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'
        mode = cool.PayloadMode.SEPARATEPAYLOAD
        muonalign_folders = [ "/MUONALIGN/MDT/BARREL", "/MUONALIGN/MDT/ENDCAP/SIDEA", "/MUONALIGN/MDT/ENDCAP/SIDEC"]
        for folder in muonalign_folders:
            remote_folder = remote_db.getFolder(folder);
            ## folder
            spec = remote_folder.folderSpecification()
            loc_folder = self._connection.createFolder(folder, spec, desc, True)
            print(f'Created locally folder {loc_folder} from remote')
        remote_db.closeDatabase()
        return "done"

    def createFolder(self, folder, spec):
        desc =  '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'
        loc_folder = self._connection.createFolder(folder, spec, desc, True)
        return loc_folder

    def createSpec(self, coldic={}):
        rspec = cool.RecordSpecification()
        for k,v in coldic.items():
            rspec.extend(k,v)
        return cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION, rspec)

    def addRow(self, record={}):
        fld = self._connection.getFolder(self._foldername)
        rspec = fld.payloadSpecification()
        r = cool.Record(rspec)
        for k,v in record.items():
           r[k] = v
        return r

    def insertIov(self, record=None):
        if record is None:
            print('Cannot insert none record')
            return
        f = self._connection.getFolder(self._foldername)
        stype = type(self._iov_since)
        print(f'Checking since of type : {stype}')
        f.storeObject( self._iov_since, self._iov_until, record, 0, self._tagname)

    def close(self):
        self._connection.closeDatabase()

#destfolder='/MUONALIGN/MDT/ENDCAP/SIDEA'
#destfolder='/MUONALIGN/MDT/ENDCAP/SIDEC'
#desttag='MuonAlignMDTEndCapAAlign-RUN3-ECA_ROLLING_2022_03_25-UPD4-02'
#desttag='MuonAlignMDTEndCapCAlign-RUN3-ECC_ROLLING_2022_03_25-UPD4-02'
#destfolder='/MUONALIGN/MDT/ASBUILTPARAMS'
#desttag='MuonAlignMdtAsbuiltparams-RUN3-UPD4-00'
#mmrec = { 'data': data, 'clobversion': '1', 'IovID': 0 }

if __name__ == '__main__':

    # Parse arguments
    parser = argparse.ArgumentParser(description='COOL sqlite generator.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['create', 'createAll', 'addiov', 'merge'], default='addiov')
    parser.add_argument('-help', '--help', action="store_true", dest="help", help='help')
    parser.add_argument('--sqlite', help='local test-align.db and CONDBR2', default='sqlite://;schema=./test-align.db;dbname=CONDBR2')
    parser.add_argument('--clob', default=None)
    parser.add_argument('--folder', default='/MUONALIGN/MDT/BARREL')
    parser.add_argument('--iov_since', default=0,
                        help='The since time')
    parser.add_argument('--iov_until', default=None,
                        help='The until time')
    parser.add_argument('--tag', default=None,
                        help='The tag name')

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    alcool = CoolTools(args.sqlite, args.folder, args.tag, args.iov_since, args.iov_until)
    alcool.connect(args.cmd)
    if args.cmd == 'create':
        alcool.copyFolder()
        print(f'Created new sqlite file: {args.sqlite}')
        exit(0)

    elif args.cmd == 'createAll':
        alcool.copyMuonAlignFolders()
        print(f'Created new sqlite file with all muonalign folders: {args.sqlite}')
        exit(0)

    elif args.cmd == 'addiov':
        if args.clob is not None:
            # Opening A/B lines file
            data = ''
            with open(args.clob, 'r') as file:
                data = file.read()
            file.close()

            print(f'loaded full file {data}')

        # Create the record
        muon_align_mrec = {'data': data, 'file': '1', 'tech': 0}

        rec = alcool.addRow(muon_align_mrec)
        alcool.insertIov(rec)
        exit(0)
    elif args.cmd == 'merge':
        print(f'ATTENTION: when generating the command you should only use the filename for --sqlite argument')
        print(f'/afs/cern.ch/user/a/atlcond/utilsflask/AtlCoolMerge.py --flask --ignoremode=BackDoor '
              f'--folder={args.folder} --tag={args.tag} --retag=$DESTTAG \"{args.sqlite}\" CONDBR2 ATONR_GPN ATLAS_COOLOFL_MUONALIGN_W $PSS')

        print(f'ATTENTION: you will need to set COOL_FLASK, DESTTAG and PSS before executing the command.')

    else:
        print(f'Cannot find command {args.cmd}')
        exit(-1)