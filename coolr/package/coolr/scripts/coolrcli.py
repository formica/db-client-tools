#!/usr/bin/env python

'''
Created on Nov 24, 2019

@author: formica
'''

import argparse
import logging
import os
import sys
import urllib.parse
from datetime import datetime

from coolr.io import CoolrDbIo
from coolr.utils import *
from coolr.utils import coolr_print, coolr_payload_print

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

#logging.basicConfig(stream=sys.stdout, level=logging.INFO,\
#        format='%(asctime)s %(levelname)s [%(name)s] %(message)s')

def compare_two_lists(reflist: list, list2: list) -> bool:
    """
    Compare two lists and logs the difference.
    :param reflist: first list.
    :param list2: second list.
    :return:      if there is difference between both lists.
    """
    print(f'Check diff among lists: reference has length {len(reflist)} and new tag has length {len(list2)}')
    diff = [i for i in list2 if i not in reflist]
    result = len(diff) == 0
    if not result:
        print(f'There are {len(diff)} differences:\n{diff[:5]}')
    else:
        print(f'No differences found !')
    return result

class CoolR(object):
    """Simple command processor example."""

    def __init__(self, server_url=None):
        if server_url is None:
            server_url = 'http://atlas-coolr-api.web.cern.ch:80/api'

        cm = None
        host = None
        loc_parser = None
        import re
        rr = r"""
            ([<|>|:]+)  # match one of the symbols
        """
        rr = re.compile(rr, re.VERBOSE)

    def set_host(self, url):
        self.host = url

    def set_parser(self, prs):
        self.loc_parser = prs

    def do_connect(self, url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        self.cm = CoolrDbIo(server_url=url)
        log.info(f'Connected to {url}')

    def do_coolcompare(self, args=None):
        """compare -s schema -n node -t sometag -tref reference_tag
        Select for payload data using the given options"""
        out = None
        response_tag = None
        response_reftag = None
        cmd = None
        db = 'CONDBR2'
        cdic = {}
        fields = []
        selnode = {}
        iovbase = 'time'
        cdic['since'] = 0
        mtag = ''
        reftag = ''
        if args:
            cmd = args['cmd']
            if 'help' in args:
                if args['help']:
                    self.loc_parser.print_help()
                    return
            if 'database' in args:
                db = args['database']
            if 'schema' in args:
                cdic['schema'] = args['schema']
            if 'node' in args:
                cdic['node'] = args['node']
            if 'tag' in args:
                mtag = args['tag']
                cdic['tag'] = mtag
            if 'tagref' in args:
                reftag = args['tagref']
            if 'channel' in args:
                cdic['chan'] = args['channel']
            if 'since' in args:
                cdic['since'] = args['since']
            if 'until' in args:
                if args['until'] == 'INF':
                    args['until'] = 9223372036854776000
                cdic['until'] = args['until']

            # load ranges
            cdic['tag'] = reftag
            response_ranges = self.cm.select(cmd='ranges', db=db, **cdic)
            ranges = self.compact_ranges(ranges=response_ranges)

            for iov in ranges:
                cdic['since'] = iov['since']
                cdic['until'] = iov['until']
                log.info(f'Check IOVs in range {iov} found in {reftag}')
                # load iovs
                response_tag = self.cm.select(cmd='iovs', db=db, **cdic)
                iovs_tag = []
                if type(response_tag) is dict:
                    out = response_tag['resources']
                    log.debug(f'Dump retrieved data : {out}')
                    for entry in out:
                        row = self.copyrow(entry, 'iovs')
                        iovs_tag.append(row)
                    #coolr_print(iovs_tag, format=fields, cmd=cmd, iovbase=iovbase, output=None)
                else:
                    log.error(f'Cannot find iovs using dictionary {cdic}')

                cdic['tag'] = reftag
                response_reftag = self.cm.select(cmd='iovs', db=db, **cdic)
                iovs_reftag = []
                if type(response_reftag) is dict:
                    out = response_reftag['resources']
                    log.debug(f'Dump retrieved data for ref tag : {out}')
                    for entry in out:
                        row = self.copyrow(entry, 'iovs')
                        iovs_reftag.append(row)
                    #coolr_print(iovs_reftag, format=fields, cmd=cmd, iovbase=iovbase, output=None)
                else:
                    log.error(f'Cannot find iovs for ref tag using dictionary {cdic}')
                log.info(f'Check IOV list differences between {mtag} and {reftag}')
                compare_two_lists(iovs_reftag, iovs_tag)

                # load payloads
                cdic['tag'] = mtag
                payloads_tag = []
                response_pyld_tag = self.cm.select(cmd='payloads', db=db, **cdic)
                if type(response_pyld_tag) is dict:
                    out = response_pyld_tag['data_array']
                    log.debug(f'Dump retrieved data : {out}')
                    for entry in out:
                        row = self.copyrow(entry, 'payloads')
                        payloads_tag.append(row)
                    ## coolr_print(payloads_tag, format=fields, cmd=cmd, iovbase=iovbase, output=None)
                else:
                    log.error(f'Cannot find payloads using dictionary {cdic}')

                cdic['tag'] = reftag
                payloads_reftag = []
                response_pyld_reftag = self.cm.select(cmd='payloads', db=db, **cdic)
                if type(response_pyld_reftag) is dict:
                    out = response_pyld_reftag['data_array']
                    log.debug(f'Dump retrieved data : {out}')
                    for entry in out:
                        row = self.copyrow(entry, 'payloads')
                        payloads_reftag.append(row)
                    ###coolr_print(payloads_reftag, format=fields, cmd=cmd, iovbase=iovbase, output=None)
                else:
                    log.error(f'Cannot find payloads using dictionary {cdic}')
                log.info(f'Check Payload list differences between {mtag} and {reftag}')
                compare_two_lists(payloads_reftag, payloads_tag)

        else:
            log.error('Cannot search ... arguments are probably missing, type -h for help')

        return

    def compact_ranges(self, ranges=[], n=1000):
        out_ranges=[]
        counter=0
        msince=0
        until=0
        for iov in ranges:
            since = iov['since']
            until = iov['until']
            if counter == 0:
                msince = since
            counter += 1
            if counter%n == 0:
                r = {'since': msince, 'until': until}
                msince = until
                out_ranges.append(r)
        # add last range
        r = {'since': msince, 'until': until}
        out_ranges.append(r)
        return out_ranges

    def copyrow(self, row={}, type='iovs'):
        """
        This command takes in input a typical COOL IOVs or Payload row and remove some keys to speed comparaison
        """
        skip = []
        newrow = {}
        if type == 'iovs':
            skip = ['channelName', 'objectid', 'tagName', 'userTagId', 'sysInstime']
            for k in row.keys():
                if k not in skip:
                    newrow[k] = row[k]
        else:
            prow = {}
            skip = ['OBJECT_ID', 'SYS_INSTIME', 'PAYLOAD_ID', 'USER_TAG_ID', 'LASTMOD_DATE', 'P_SYS_INSTIME']
            for k in row.keys():
                if k not in skip:
                    newrow[k] = row[k]
            rh = hash(frozenset(newrow.items()))
            prow['IOV_SINCE'] = newrow['IOV_SINCE']
            prow['phash'] = rh
            newrow = prow

        return newrow

    def do_select(self, args=None):
        """select [schemas|tail|nodes|tags|channels|iovs|payloads] -s schema -n node -t sometag
        Select for data using the given options"""
        out = None
        response = None
        cmd = None
        db = 'CONDBR2'
        cdic = {}
        fields = []
        selnode = {}
        iovbase = 'time'
        if args:
            cmd = args['cmd']
            if 'help' in args:
                if args['help']:
                    self.loc_parser.print_help()
                    return
            if 'database' in args:
                db = args['database']
            if 'schema' in args:
                cdic['schema'] = args['schema']
            if 'node' in args:
                cdic['node'] = args['node']
            if 'tag' in args:
                cdic['tag'] = args['tag']
            if 'globaltag' in args:
                cdic['name'] = args['globaltag']
            if cmd == 'backtrace':
                tagname = args['tag']
                cdic['name'] = tagname
                cdic.pop('tag', None)
            if cmd in ['iovs', 'payloads', 'tail']:
                if 'channel' in args:
                    cdic['chan'] = args['channel']
                if 'since' in args:
                    cdic['since'] = args['since']
                if 'until' in args:
                    if args['until'] == 'INF':
                        args['until'] = 9223372036854776000
                    cdic['until'] = args['until']
                if 'iovtype' in args:
                    cdic['iovtype'] = args['iovtype']
                nodedic = {'schema': args['schema'], 'node': args['node'], 'db': db}
                nodelist = self.cm.select(cmd='nodes', db=db, **cdic)
                selnode = nodelist[0]
                iovbase = selnode['nodeIovBase']
                log.debug(f'Found node: {selnode} of iov type {iovbase}')
                # print(f'IovBase for node {selnode} is {iovbase}')
            if 'fields' in args:
                log.debug('fields is %s' % args.get('fields'))
                if 'none' == args['fields']:
                    fields = []
                elif 'help' == args['fields']:
                    print_help(cmd)
                    return
                else:
                    fields = args.get('fields').split(',')
                    log.info(f'Print columns {fields}')

            if cmd == 'payloads':
                cdic['qrytype'] = 'none'

            log.debug(f'Retrieve data using {cmd} and dictionary {cdic}')
            response = self.cm.select(cmd=cmd, db=db, **cdic)
            if type(response) is dict:
                if 'resources' in response.keys():
                    out = response['resources']
                elif 'data_array' in response.keys():
                    out = response['data_array']
            else:
                out = response

        else:
            log.error('Cannot search ... arguments are probably missing, type -h for help')

        outfname = None
        if 'output' in args:
            if args['output'] != 'none':
                outfname = args['output']
        log.debug(f'Setting for output is {outfname}')
        if 'skip' in args and args['skip']:
            log.info(f'Found output list for {cmd} of size {len(out)}')
        elif 'payloadfmt' in args and args['payloadfmt']:
            log.info(f'Printing payload {args["payloadfmt"]}')
            coolr_payload_print(out, format=fields, cmd=cmd, iovbase=iovbase, output=outfname, dumpfield=args['dumpfield'])
        else:
            log.info(f'Dump retrieved data : {out}')
            coolr_print(out, format=fields, cmd=cmd, iovbase=iovbase, output=outfname)
        return out

    def do_convert(self, line):
        """convert date
        Convert a date to UTC unix time."""
        dt = datetime.fromisoformat(line)
        log.info('create time from string %s %s' % (line, dt.timestamp()))
        since = int(dt.timestamp() * 1000)
        print(f'date {line} = {since}')

    def do_nemop(self, args=None):
        """Get runs list from nemop."""
        cdic = {}
        fields = []
        if 'fields' in args:
            fields = args['fields'].split(',')
        outfname = None
        if 'output' in args:
            outfname = args['output']
        cmd = args['cmd']
        search_cond = urllib.parse.quote('run>%s,run<%s' % (args['runmin'], args['runmax']), safe=',')
        search_cond = 'run>%s,run<%s' % (args['runmin'], args['runmax'])
        cdic = {'by': search_cond}
        log.info(f'Search run range by={search_cond}')
        out = self.cm.search_runs(cmd=cmd, page=args['page'], size=args['size'], sort=args['sort'], **cdic)
        coolr_print(out, format=fields, cmd=cmd, output=outfname)
        return out

    def do_coolcopy(self, args=None):
        """Get a list of AtlCoolCopy commands to dump tags which are associated to a global tag.
        The purpose is to easily generate a DB release based on a global tag.
        """
        cdic = {}
        outfname = 'dbrelease.sh'
        # Save the original cmd name here and
        cmd = args['cmd']
        args['cmd'] = 'trace'
        globaltag = args['globaltag']
        dbname = args['database']
        log.info(f'Search tags associated to global tag {globaltag} in COOL instance {dbname}')
        out = self.do_select(args)
        """
             echo "Copy folder MV "; 
             AtlCoolCopy.exe "oracle://ATLAS_COOLPROD;schema=ATLAS_COOLOFL_TILE;dbname=OFLP200;user=ATLAS_COOL_READER;password=${READER_PASS}" 
             "sqlite://;schema=./ATLAS_COOLOFL_TILE.sqlite;dbname=OFLP200" 
             -create -cti -hi -tag TileOfl02CalibEms-COM-00 -outtag TileOfl02CalibEms-COM-00 -f /TILE/OFL02/CALIB/EMS -of /TILE/OFL02/CALIB/EMS -readoracle
        """
        coolr_coolcopy_print(out, dbname=args['database'], cmd='trace', output=outfname)
        return


    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            # Configuration

            # Remove this if you don't plan to "deactivate" the proxy later
            #        default_socket = socket.socket
            # Set up a proxy
            #            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            log.info('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            log.error('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

# coolrcli.py
def main():
    print("Command Line Interface for COOLR")
    # Parse arguments
    parser = argparse.ArgumentParser(description='Coolr browser.', add_help=False)
    parser.add_argument('--cmd', choices=['schemas', 'nodes', 'tags', 'iovs', 'payloads', 'globaltags', 'trace',
                                          'tail', 'backtrace', 'run', 'calib', 'cooldump', 'compare', 'ranges'],
                        default='schemas')
    parser.add_argument('--host', default='atlas-coolr-api.web.cern.ch',
                        help='Host of the COOLR service (default: atlas-coolr-api.web.cern.ch)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api)')
    parser.add_argument('--port', default='80',
                        help='Port of the COOLR service (default: 80)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    parser.add_argument('--payloadfmt', action='store_true',
                        help='Dump payload as dictionary (default: false)')
    parser.add_argument("-d", "--database", default='CONDBR2', help="the database: CONDBR2, COMP200, OFLP200")
    parser.add_argument("-s", "--schema", help="the schema name")
    parser.add_argument("-n", "--node", help="the node name")
    parser.add_argument("-t", "--tag", help="the tag name")
    parser.add_argument("-tref", "--tagref", help="the reference tag name to be used in the compare method")
    parser.add_argument("-g", "--globaltag", help="the global tag name")
    parser.add_argument("--iovtype", default='cooltime', help="the iov type: cooltime, time, run-lumi")
    parser.add_argument("--runmin", help="the start run number")
    parser.add_argument("--runmax", default="999999", help="the end run number")
    parser.add_argument("--since", help="the since time")
    parser.add_argument("--until", default="INF", help="the until time")
    parser.add_argument("--channel", default='%', help="the channel id or a name pattern")
    parser.add_argument("--output", default="none", help="write json data into the provided output file")
    parser.add_argument("--page", default="0", help="the page number. Only applicable to 'run' command.")
    parser.add_argument("--size", default="1000", help="the page size. Only applicable to 'run' command.")
    parser.add_argument("--sort", default="run:ASC",
                        help="the sort parameter (depend on the selection). Only applicable to 'run' command.")
    parser.add_argument("--dumpfield", default="none",
            help="When used with payloadfmt, dumps a file to disk with the content of this field")

    parser.add_argument('--skip', action="store_true", help='skip print out, only summary is printed')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-f", "--fields", default='none',
                        help="""
                        the list of fields to show, separated with a comma. Use -f help to get the available fields.
                        When requesting the payload instead this can be used to shorten the column list, as an exclusion filter.
                        A quick way to reduce the output is to put -f short .
                        """)
    parser.add_argument("-H", "--header", default="BLOB", help="set header request for payload: BLOB, JSON, ...")

    args = parser.parse_args()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host
    ui = CoolR()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    ui.set_parser(parser)
    argsdic = vars(args)
    print(f'Parser found arguments : {args} {argsdic}')
    if args.fields:
        if args.fields == 'help':
            log.info(f'Getting help for type {args.cmd}')
            print_help(args.cmd)
            sys.exit()
    if args.cmd in ['run']:
        log.info(f'Launch run search command {args.cmd}')
        ui.do_nemop(argsdic)
    elif args.cmd in ['cooldump']:
        log.info(f'Launch COOL dump command {args.cmd}')
        ui.do_coolcopy(argsdic)
    elif args.cmd in ['compare']:
        log.info(f'Launch COOL compare command {args.cmd}')
        ui.do_coolcompare(argsdic)
    else:
        log.info(f'Launch command {args.cmd}')
        ui.do_select(argsdic)

if __name__ == '__main__':
    main()
