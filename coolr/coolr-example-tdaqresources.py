#!/usr/bin/env python

"""
Created on March 12, 2021

@author: formica
"""
from datetime import datetime
import logging
import pandas as pd

from coolr.io import CoolrDbIo
from coolr.utils import *

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

def socks():
    SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
    SOCKS5_PROXY_PORT = 3129
    try:
        import socket
        import socks  # you need to install pysocks (use the command: pip install pysocks)
        # Configuration

        # Remove this if you don't plan to "deactivate" the proxy later
        #        default_socket = socket.socket
        # Set up a proxy
        #            if self.useSocks:
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
        print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
    except:
        print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

def get_data(mcmd='iovs', mdb='CONDBR2', select_dic={}, cm=None):
    resp_payload = cm.select(cmd=mcmd, db=mdb, **select_dic)
    if resp_payload is None:
        log.error(f'No data retrieved for {mcmd} {select_dic}')
        return None
    if mcmd == 'channels':
        return resp_payload
    arr_size = len(resp_payload['resources'])
    log.info(f'Retrieved {mcmd} {arr_size}')
    return resp_payload

if __name__ == '__main__':

    schema = 'ATLAS_COOLONL_TDAQ'
    node_mmg = '/TDAQ/Resources/ATLAS/MUON_MMG/ELinks'
    node = '/TDAQ/RunCtrl/EOR'
    runsince = '452590-0'
    rununtil = '452590-0'
    iovtype = 'run-lumi'

    select_run_dic = {
        'schema': schema,
        'node': node,
        'since': runsince,
        'until': rununtil,
        'iovtype': iovtype
    }

    select_chan_dic = {
        'schema': schema,
        'node': node_mmg
    }

    socks()

    cm = CoolrDbIo(server_url='http://atlas-coolr-api.web.cern.ch/api')

    cool_eor_payload = cm.select(cmd='payloads', db='CONDBR2', **select_run_dic)
    arr_size = len(cool_eor_payload['data_array'])
    select_dic = {}
    for arow in cool_eor_payload['data_array']:
        since = arow['IOV_SINCE']
        runnum = arow['RunNumber']
        sor = arow['SORTime']
        eor = arow['EORTime']
        log.info(f'== SOR={sor}, EOR={eor} run {runnum}')
        select_dic = {
            'schema': schema,
            'node': node_mmg,
            'since': sor,
            'until': eor,
            'iovtype': 'time'
        }   

    resp_payload = get_data(mcmd='channels', mdb='CONDBR2', select_dic=select_chan_dic, cm=cm)
    channels_list = []
    for arow in resp_payload:
        channel = arow['channelId']
        chname = arow['channelName']
        if 'MM' in chname:
            channels_list.append({channel: chname})

    log.info(f'== Found {len(channels_list)} channels')
    resp_payload = get_data(mcmd='iovs', mdb='CONDBR2', select_dic=select_dic, cm=cm)
    if resp_payload is None:
        log.error(f'No data retrieved for {mcmd} {select_dic}')
        sys.exit(1)

    out_dic_list = []
    for arow in resp_payload['resources']:
        since = arow['iovSince']
        channel = arow['channelId']
        chname = arow['channelName']
        until = arow['iovUntil']
        log.info(f'== SINCE={since} until={until} [channel={channel}, name={chname}]')
        out_dic_list.append({'since': since, 'until': until, 'channel': channel, 'name': chname})

    pd.DataFrame(out_dic_list).sort_values(by=['channel', 'since']).to_csv('mmg.csv', index=False)