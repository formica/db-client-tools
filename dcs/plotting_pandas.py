import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from dcs_pandas_tools import DcsPandasTools

dcs = DcsPandasTools(passwd="xxx",sincet=1540854000000000000,untilt=1540855000000000000)
df = dcs.readSqlLarge(1000)

dft=df.groupby(['CHANNEL_ID']).aggregate(np.mean)
dft.head()
dft.plot(kind='scatter',x='v0set_ML1',y='iMon_ML1',color='red')
plt.show()

dft.describe()
dft
for col in dft.columns:
    print(col)

dft=df.groupby(['CHANNEL_ID'],as_index=False).aggregate(np.mean)
for col in dft.columns:
    print(col)

dft.plot(kind='bar',x='CHANNEL_ID',y='iMon_ML1')
plt.show()
dft.plot(kind='scatter',x='CHANNEL_ID',y='iMon_ML1',color='red')
plt.show()
df_mdt = df
dft=df_mdt.groupby(['CHANNEL_ID', pd.to_datetime(df_mdt['SINCET'], unit='ms')],as_index=False).aggregate(np.mean)
dft.head()
dft=df_mdt.groupby(['CHANNEL_ID', pd.to_datetime(df_mdt['SINCET'], unit='ms').dt.floor('H')],as_index=False).aggregate(np.mean)
dft.head()
dft.plot(kind='scatter',x='SINCET',y='iMon_ML1',color='red')
plt.show()
dft=df_mdt.groupby(['CHANNEL_ID', pd.to_datetime(df_mdt['SINCET'], unit='ms').dt.floor('H')],as_index=False).aggregate(np.mean)
dft.head()

# use df with chan 4
df_ch4=df_mdt[df_mdt['CHANNEL_ID'] == 4]
dft=df_ch4.groupby(['CHANNEL_ID', pd.to_datetime(df_mdt['SINCET'], unit='ms').dt.floor('H')],as_index=False).aggregate(np.mean)
df_ch4.head()
