# coding: utf-8
import cx_Oracle
import argparse

class EcMdtTools(object):

    ora_conn = {
        'host': 'atonr-rac54-scan.cern.ch',
        'port': 10121,
        'service': 'atonr_adg.cern.ch',
        'schema': 'ATLAS_MDT_DCS_R',
        'offset': 0,
        'limit': 10000
    }

    mdt_maxcyle_qry = """
        select max(cycle_number) as mxcyc 
        from  atlas_mdt_dcs.%s partition(%s_%s_%d)
    """
    mdt_count_qry = """
        select count(*) as nrowsb, max(stime) as ist,TO_CHAR(UNIXTS_TO_DATE(max(stime)),'DD-MM-YYYY HH24:MI:SS' )  as stt 
    from atlas_mdt_dcs.%s  partition(%s_%s_%d) where cycle_number=:cycnum
    """

    def __init__(self, passwd=None, month=None, year=None, table=None):  # noqa: E501
        self._passwd = passwd
        self._table = 'AL_%s' % table
        self._month = month
        self._year = year
        #self._dsn = cx_Oracle.makedsn(host=self.ora_conn['host'],port=self.ora_conn['port'],service_name=self.ora_conn['service'])
        self._connection = cx_Oracle.connect(self.ora_conn['schema'], self._passwd, 'ATONR_ADG' )
        print('Initialised oracle connection')

    def read_count(self, cond=None):
        qry = self.mdt_maxcyle_qry % (self._table, self._table, self._month, self._year)
        print('Initialised request to DB : %s ' % qry)
        # Obtain a cursor
        cursor = self._connection.cursor()
        cursor.execute(qry)
        res = cursor.fetchone()
        maxcount = 0
        for row in res:
            print(f'Found {row}')
            maxcount = row

        print(f'Search number of readings for cycle: {maxcount}')
        qry = self.mdt_count_qry % (self._table, self._table, self._month, self._year)
        if cond is not None:
            qry = '%s and %s' % (qry, cond)
        cursor.execute(qry, cycnum=maxcount)
        rescount = cursor.fetchall()
        for row in rescount:
            print(f'Found {row}')

if __name__ == '__main__':

    if __name__ == '__main__':
        # Parse arguments
        parser = argparse.ArgumentParser(description='Ec counter.', add_help=False)
        parser.add_argument('--table', choices=['T', 'B', 'R', 'D'],
                            default='AL_T')
        parser.add_argument('--password', default='none',
                            help='The reader password')
        parser.add_argument('--month', choices=['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL',
                                                'AUG', 'SEP', 'NOV', 'DEC'], default='FEB',
                            help='The month for the partition to choose')
        parser.add_argument('--year', default=2022,
                            help='The year for the partition to choose')
        parser.add_argument('--ec', action='store_true',
                        help='Add condition for X_EC<0')

    args = parser.parse_args()

    ecoracle = EcMdtTools(args.password, args.month, int(args.year), args.table)
    cname = None
    if args.ec:
        cname = '%s_EC<0' % args.table

    ecoracle.read_count(cond=cname)