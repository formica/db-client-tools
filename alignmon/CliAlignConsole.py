'''
Created on Feb 28, 2020

@author: formica
'''

import argparse
import atexit
import cmd
import logging
import os
import readline
import sys
from datetime import datetime

from aligncli import AlignCli

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

sys.path.append(os.path.join(sys.path[0], '..'))
historyFile = '.alignconsole_hist'


class AlignmonConsoleUI(cmd.Cmd):
    """Simple command processor example."""
    cm = None
    prompt = '(MuonAlign): '
    homehist = os.getenv('CDMS_HISTORY_HOME', os.environ["HOME"])
    histfile = os.path.join(homehist, historyFile)
    host = None
    loc_parser = None
    import re
    rr = r"""
        ([<|>|:]+)  # match one of the symbols
    """
    rr = re.compile(rr, re.VERBOSE)

    def init_history(self, histfile):
        readline.parse_and_bind("tab: complete")
        readline.set_history_length(100)
        if hasattr(readline, "read_history_file"):
            try:
                readline.read_history_file(histfile)
            except IOError:
                pass
            atexit.register(self.save_history, histfile)

    def save_history(self, histfile):
        log.info('Saving history in %s' % histfile)
        readline.write_history_file(histfile)

    def set_host(self, url):
        self.host = url

    def get_args(self, line=None):
        from shlex import split
        argv = split(line)
        self.loc_parser = argparse.ArgumentParser(description="parser options for cli", add_help=False)
        group = self.loc_parser.add_mutually_exclusive_group()
        group.add_argument("-v", "--verbose", action="store_true")
        group.add_argument("-q", "--quiet", action="store_true")
        #        self.loc_parser.add_argument('cmd', nargs='?', default='iovs')
        self.loc_parser.add_argument('--type', choices=['tags', 'opticallines'], default='tags')
        self.loc_parser.add_argument('-h', '--help', action="store_true", help='show this help message')
        self.loc_parser.add_argument("-t", "--tag", help="the tag name")
        self.loc_parser.add_argument("--olname", help="the optical line name")
        self.loc_parser.add_argument("--params", help="the string containing k=v pairs for objects creation")
        self.loc_parser.add_argument("--inpfile", help="the input file to upload")
        self.loc_parser.add_argument("--since", help="the since time for the payload.")
        self.loc_parser.add_argument("--page", default="0", help="the page number.")
        self.loc_parser.add_argument("--size", default="100", help="the page size.")
        self.loc_parser.add_argument("--sort", default="none", help="the sort parameter (depend on the selection).")
        self.loc_parser.add_argument("--fields", default='none',
                                     help="the list of fields to show, separated with a comma. Use --fields help to get the available fields.")
        self.loc_parser.add_argument("-f", "--format", help="the output format, use <help> for details")
        self.loc_parser.add_argument("-H", "--header", default="BLOB",
                                     help="set header request for payload: BLOB, JSON, ...")
        return self.loc_parser.parse_args(argv)

    def do_connect(self, url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        self.cm = AlignCli(server_url=url)
        self.cm.set_host(url)
        self.cm.do_connect()
        log.info(f'Connected to {url}')

    def do_info(self, line):
        """info <datatype> : provide information on the content of a given data type
         datatype: opticallines, tags.
         """
        largs = None
        if line:
            largs = self.get_args(line)
            if largs.help:
                self.loc_parser.print_help()
                return
        argsdic = vars(largs)
        self.cm.do_info(argsdic)

    def do_ls(self, line):
        """ls <datatype> [-t tag_name] [--olname opticalline_name] [other options: --size, --page, --sort, --format]
         Search for data collection of different kinds: opticallines, tags.
         datatype: opticallines, tags.
         Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
         """
        largs = None
        if line:
            largs = self.get_args(line)
            if largs.help:
                self.loc_parser.print_help()
                return
        argsdic = vars(largs)
        self.cm.do_ls(argsdic)

    def do_create(self, line):
        """create <datatype> [-t tag_name] [--olname opticalline_name] [other options: --params "k=v,k1=v1,"]
         Create entry for datatype: opticallines, tags.
         datatype: opticallines, tags.
         Type create -h for help on available options (not all will be applicable depending on the chosen datatype)
         """
        largs = None
        if line:
            largs = self.get_args(line)
            if largs.help:
                self.loc_parser.print_help()
                return
        argsdic = vars(largs)
        self.cm.do_create(argsdic)

    def do_convert(self, line):
        """convert date
        Convert a date to UTC unix time."""
        dt = datetime.fromisoformat(line)
        log.info('create time from string %s %s' % (line, dt.timestamp()))
        since = int(dt.timestamp() * 1000)
        print(f'date {line} = {since}')

    def do_exit(self, line):
        return True

    def do_quit(self, line):
        return True

    def emptyline(self):
        pass

    def preloop(self):
        self.init_history(self.histfile)

    def postloop(self):
        print

    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            # Configuration

            # Remove this if you don't plan to "deactivate" the proxy later
            #        default_socket = socket.socket
            # Set up a proxy
            #            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Alignmon browser.')
    parser.add_argument('--host', default='localhost',
                        help='Host of the Alignmon service (default: localhost)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api)')
    parser.add_argument('--port', default='8080',
                        help='Port of the Alignmon service (default: 8080)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    args = parser.parse_args()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host
    ui = AlignmonConsoleUI()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info(
            "Activating socks on localhost:3129 ; if you want another address please set CDMS_SOCKS_HOST and _PORT env vars")
        ui.socks()

    ui.cmdloop()
