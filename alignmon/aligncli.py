'''
Created on Nov 24, 2017

@author: formica
'''

import argparse
import logging
import sys, os
from alignmon.cli.climgr import AlignCli
from alignmon.utils.alignutils import *

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Align browser.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['ls', 'create', 'dump', 'amdb'], default='ls')
    parser.add_argument('--type', choices=['tags', 'iovs', 'corrections', 'fitsteps',
                                           'cooltags', 'opticallines', 'imtags', 'imiovs', 'imconfigs'], default='tags')
    parser.add_argument('--host', default='localhost',
                        help='Host of the Align service (default: aiatlas061.cern.ch)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api)')
    parser.add_argument('--port', default='8080',
                        help='Port of the AlignMon service (default: 8080)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-m", "--mode", help="the mode to request intervalmaker iovs: [history, execute, store]",
                        default="history")
    parser.add_argument("-t", "--tag", help="the tag name")
    parser.add_argument("-c", "--cooltag", help="the cool tag name")
    parser.add_argument("-i", "--iovid", default=None, help="the IovID.")
    parser.add_argument("--olname", help="the opticalline name")
    parser.add_argument("--params", help="the string containing k=v pairs for tags or global tags creation")
    parser.add_argument("--cut", help="additional selection parameters. e.g. since>1000,until<2000")
    parser.add_argument("--inpfile", help="the input file to upload")
    parser.add_argument("--since", help="the since time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00")
    parser.add_argument("--until", help="the until time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00")
    parser.add_argument("--corrections", help="the list of files to upload: alines=alines.txt,blines=blines.txt,"
                                              "fitsteps=fs.txt,pulls=pulls.txt")
    parser.add_argument("--amdb", help="the list of files to merge: alines=alines.txt,blines=blines.txt")
    parser.add_argument("--side", default='NONE', help="the Endcap Side [A|C].")
    parser.add_argument("--page", default="0", help="the page number.")
    parser.add_argument("--size", default="100", help="the page size.")
    parser.add_argument("--sort", default="none", help="the sort parameter (depend on the selection).")
    parser.add_argument("-f", "--fields", default='none',
                        help="the list of fields to show, separated with a comma. Use -f help to get the available fields.")
    parser.add_argument("-H", "--header", default="BLOB", help="set header request for payload: BLOB, JSON, ...")

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host
    ui = AlignCli()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    ui.set_parser(parser)
    argsdic = vars(args)
    if args.fields:
        if args.fields == 'help':
            if args.type == 'tags':
                print(f'Fields for {args.cmd} are {tagfieldsdicheader.keys()}')
            else:
                print('not available for this command')
            sys.exit()
    if args.params:
        if args.params == 'help':
            print_help(args.type)
            sys.exit()
    if args.corrections:
        if args.corrections == 'help':
            print_corrections()
            sys.exit()
    if args.cmd in ['ls']:
        log.info(f'Launch ls command on {args.type}')
        ui.do_ls(argsdic)
    elif args.cmd in ['create']:
        log.info(f'Launch create command on {args.type}')
        ui.do_create(argsdic)
    elif args.cmd in ['dump']:
        log.info(f'Launch dump command on {args.type}')
        ui.do_dump(argsdic)
    elif args.cmd in ['amdb']:
        log.info(f'Read amdb files to create CLOB in ascii format')
        ui.do_amdb(argsdic)
    else:
        log.info(f'Cannot launch command {args.cmd}')
