from ..io.aligndbio import AlignDbIo
from datetime import datetime
from ..utils.alignutils import *
import os
# Log
import logging

log = logging.getLogger('cli_client')
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

print('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__, __name__, str(__package__)))


class AlignCli(object):
    """Simple command processor example."""

    def __init__(self, server_url=None, use_tokens=True):
        if server_url is None:
            server_url = 'http://localhost:8080/api'

        cm = None
        self.host = server_url
        self._use_tokens = use_tokens
        loc_parser = None
        import re
        self.rr = r"""
            ([<|>|:]+)  # match one of the symbols
        """
        self.rr = re.compile(self.rr, re.VERBOSE)

    def set_host(self, url):
        self.host = url

    def set_parser(self, prs):
        self.loc_parser = prs

    def do_connect(self, url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        self.cm = AlignDbIo(server_url=url, max_tries=1, use_tokens=self._use_tokens)
        log.info(f'Connected to {url}')

    def do_dump(self, args=None):
        """dump <datatype> [-i iovid]
        Dump data for a given iovid: iovs.
        datatype: iovs.
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = 'iovs'
        tagname = None
        cooltagname = None
        optline = '%'
        iovid = None
        cdic = {}
        if args:
            print(f'Received args {args}')
            if 'type' in args:
                cmd = args['type']
            log.info(f'Searching for data of type {cmd}')
            if 'iovid' in args:
                iovid = args['iovid']
        if cmd == 'iovs':
            if iovid is None:
                log.error('Cannot dump iov {iovid}')
                return
            log.info(f'Get iov using {iovid}')
            out = self.cm.get_iov_file(iovid=iovid, format='COOL')
            print(f'Found file {out}')
            return out
        return None

    def do_ls(self, args=None):
        """ls <datatype> [-t tag_name] [--olname opticalline_name] [other options: --size, --page, --sort, --format]
        Search for data collection of different kinds: opticallines, tags.
        datatype: opticallines, iovs, tags.
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = 'tags'
        tagname = None
        cooltagname = None
        optline = '%'
        iovid = None
        cdic = {}
        pdic = {}
        since = None
        until = None
        if args:
            print(f'Received args {args}')
            if 'type' in args:
                cmd = args['type']
            log.info(f'Searching for data of type {cmd}')
            if 'tag' in args:
                tagname = args['tag']
            if 'cooltag' in args:
                cooltagname = args['cooltag']
            if 'olname' in args:
                optline = args['olname']
            if 'iovid' in args:
                iovid = args['iovid']
            # Init general sort, since, until only if they are present
            sort = args['sort']
            if 'since' in args and args['since'] is not None:
                st = args['since']
                since = datetime.strptime(st, '%Y-%m-%d %H:%M:%S')
            if 'until' in args and args['until'] is not None:
                tt = args['until']
                until = datetime.strptime(tt, '%Y-%m-%d %H:%M:%S')

            if 'cut' in args and args['cut'] is not None:
                cutstringarr = args['cut'].split(',')
                for el in cutstringarr:
                    ss = self.rr.findall(el)
                    ##print(el,ss)
                    (k, v) = el.split(ss[0])
                    cdic[k] = f'{ss[0]}{v}'
                log.info('use cut params : %s' % cdic)

            if 'params' in args and args['params'] is not None:
                pararr = args['params'].split(',')
                for par in pararr:
                    kv = par.split('=')
                    pdic[kv[0]] = kv[1]
            fields = []
            if 'fields' in args:
                log.debug('fields is %s' % args.get('fields'))
                if 'none' == args['fields']:
                    fields = []
                else:
                    fields = args.get('fields').split(',')

            # Check the data type and search
            if cmd == 'tags':
                tagtype = None
                if tagname is None:
                    tagname = "%"
                # Use the params option to get further arguments.
                if 'type' in pdic:
                    tagtype = pdic['type']
                log.info(f'Search tags using {tagname} and {tagtype}')
                out = self.cm.search_tags(tagPattern=tagname, type=tagtype)
                out['format'] = 'AligntagSetDto'
            elif cmd == 'cooltags':
                log.info(f'Search cool tags  using {cooltagname}')
                if cooltagname is None:
                    cooltagname = "%"
                cdic['name'] = cooltagname
                if "none" == sort:
                    sort = "pk.coolTag:ASC"
                out = self.cm.search_cooltags(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AlignCoolTagSetDto'
            elif cmd == 'iovs':
                log.info(f'Search iovs using {tagname}')
                if tagname is None:
                    log.error('Cannot search iovs without a tagname specified')
                    return
                if "none" == sort:
                    sort = "iovId:ASC"
                out = self.cm.search_iovs(tagname=tagname, since=since, until=until,
                                          page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AligniovSetDto'
            elif cmd == 'corrections':
                log.info(f'Search corrections using {tagname}')
                if tagname is None:
                    log.error('Cannot search corrections without a tagname specified')
                    return
                if "none" == sort:
                    sort = "hwElement:ASC"
                cdic['iovId'] = iovid
                cdic['tagname'] = tagname
                out = self.cm.search_corrections(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AligncorrectionsSetDto'
            elif cmd == 'fitsteps':
                log.info(f'Search fitsteps')
                if "none" == sort:
                    sort = "fitId:ASC"
                cdic['iovId'] = iovid
                cdic['tagname'] = tagname
                out = self.cm.search_fitsteps(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AlignFitStepsSetDto'
            elif cmd == 'fitpulls':
                log.info(f'Search fitpulls using {tagname}')
                if tagname is None:
                    log.error('Cannot search fitsteps without a tagname specified')
                    return
                if "none" == sort:
                    sort = "pk.olname:ASC"
                cdic['iovId'] = iovid
                cdic['tagname'] = tagname
                out = self.cm.search_fitpulls(page=args['page'], size=args['size'], sort=sort, **cdic)
                out['format'] = 'AlignFitPullsSetDto'
            elif cmd == 'opticallines':
                cdic['olname'] = optline
                log.info(f'Search opticallines using {optline}')
                out = self.cm.search_opticallines(**cdic)
                out['format'] = 'OpticallinesSetDto'
            elif cmd == 'imtags':
                log.info(f'Search intervalmaker tags using {tagname}')
                out = self.cm.search_im_tags(tagname=tagname)
                out['format'] = 'IntervalMakerTagSetDto'
            elif cmd == 'imconfigs':
                log.info(f'Search intervalmaker configurations using {tagname}')
                out = self.cm.search_im_configurations(tagname=tagname)
                out['format'] = 'IntervalMakerConfigurationSetDto'
            elif cmd == 'imiovs':
                log.info(f'Search intervalmaker iovs using {tagname}')
                out = self.cm.search_im_iovs(mode=args['mode'], tagname=tagname, since=cdic['since'],
                                             until=cdic['until'], fmt='iso')
                out['format'] = 'IntervalMakerIovSetDto'
            else:
                print(f'Command {cmd} is not recognized in this context')
        server_print(out, format=fields)
        return out

    def do_info(self, args):
        """info <datatype>
         Provide help for k=val pairs to be used in params for the requested datatype
         """
        if args:
            cmd = None
            if 'type' in args:
                cmd = args['type']
            if cmd == 'tags':
                print(f'Fields for {cmd} are {tagfieldsdicheader.keys()}')
            elif cmd == 'opticallines':
                print(f'Fields for {cmd} are {optlinefieldsdicheader.keys()}')
            else:
                print('not available for this command')

    def do_amdb(self, args):
        """amdb
         Provide help for k=val pairs to be used in amdb
         """
        out = None
        if args:
            print(args)
            amdbdic = {}
            if 'amdb' in args and args['amdb'] is not None:
                amdbarr = args['amdb'].split(',')
                for par in amdbarr:
                    kv = par.split('=')
                    print(f'par= {par} kv= {kv}')
                    amdbdic[kv[0]] = kv[1]
            cmd = args['cmd']
            if cmd == 'amdb':
                dataA = None
                dataB = None
                if 'alines' in amdbdic and amdbdic['alines'] is not None:
                    dataA = read_AB_lines(amdbdic['alines'])
                if 'blines' in amdbdic and amdbdic['blines'] is not None:
                    dataB = read_AB_lines(amdbdic['blines'])
                if dataA is not None:
                    ii = int(0)
                    corr_ab = merge_corrections(dataA, dataB)
                    for line in corr_ab:
                        line['corrId'] = int(ii)
                        line['corrDescription'] = 'AB'
                        ii += 1
                    log.info(f'Creating new set of corrections as blob list of size {len(corr_ab)}')
                    out = print_corrections_clob(corr_ab)
            else:
                log.warning('Cannot create object without arguments')
            dump_clob('out.txt', out)
        print(f'Dump clob in out.txt')

    def do_create(self, args):
        """create <datatype> k=v,k1=v1,....
        Create a new tag or cool tag, using a series of k=val pairs, separated
        by commas
        """
        out = None
        cmd = None
        pdic = {}
        tname = None
        cooltname = None
        optline = None
        iovid = None
        if args:
            if 'type' in args:
                cmd = args['type']
            log.info(f'Creating object for type {cmd}')
            if 'tag' in args and cmd in ['tags', 'iovs', 'cooltags', 'imconfigs', 'imiovs', 'imtags']:
                tname = args['tag']
            if 'cooltag' in args and cmd in ['cooltags', 'cooliovs']:
                cooltname = args['cooltag']
            if 'iovid' in args and cmd in ['iovs', 'corrections']:
                iovid = args['iovid']
            if 'olname' in args and cmd in ['opticallines']:
                optline = args['olname']

            # Check if the user needs hints on fields
            if 'params' in args and args['params'] is not None:
                pararr = args['params'].split(',')
                for par in pararr:
                    kv = par.split('=')
                    pdic[kv[0]] = kv[1]
            # Check if the user needs hints on fields
            corrdic = {}
            if 'corrections' in args and args['corrections'] is not None:
                corrarr = args['corrections'].split(',')
                for par in corrarr:
                    kv = par.split('=')
                    corrdic[kv[0]] = kv[1]
            if cmd == 'tags':
                log.info(f'Creating tag {tname} and args {pdic}')
                if tname is None:
                    print(f"Cannot create tag without the name provided via -t or --tag")
                    return
                out = self.cm.create_tags(name=tname, **pdic)
            elif cmd == 'cooltags':
                log.info(f'Creating cooltag {cooltname} associate to align tag {tname} and args {pdic}')
                if cooltname is None or tname is None:
                    print(
                        f"Cannot create cooltag without the name of a cool and align tags provided via --tag and --cooltag")
                    return
                out = self.cm.create_cooltags(name=cooltname, aligntag=tname, **pdic)
            elif cmd == 'opticallines':
                log.info(f'Creating opticallines {optline} and args {pdic}')
                if optline is None:
                    print(f"Cannot create opticalline without the name provided via --olname")
                    return
                out = self.cm.create_opticallines(name=optline, **pdic)
            elif cmd == 'iovs':
                since = None
                until = None
                if 'since' in args:
                    st = args['since']
                    since = datetime.strptime(st, '%Y-%m-%d %H:%M:%S')
                if 'until' in args:
                    tt = args['until']
                    until = datetime.strptime(tt, '%Y-%m-%d %H:%M:%S')
                log.info(f'Creating new iov for tag {tname} time {since} and {until} and args {pdic}')
                out = self.cm.create_iovs(name=tname, since=since, until=until, **pdic)
            elif cmd == 'corrections':
                dataA = None
                dataB = None
                dataFS = None
                dataFP = None
                if 'alines' in corrdic and corrdic['alines'] is not None:
                    dataA = read_AB_lines(corrdic['alines'])
                if 'blines' in corrdic and corrdic['blines'] is not None:
                    dataB = read_AB_lines(corrdic['blines'])
                if 'fitsteps' in corrdic and corrdic['fitsteps'] is not None:
                    dataFS = read_fitsteps(corrdic['fitsteps'])
                if 'fitpulls' in corrdic and corrdic['fitpulls'] is not None:
                    dataFP = read_fitpulls(corrdic['fitpulls'])
                if dataA is not None:
                    corr_ab = merge_corrections(dataA, dataB)
                    log.info(
                        f'Creating new set of corrections for iov {iovid} and corrections list of size {len(corr_ab)}')
                    out = self.cm.create_corrections(iovid=iovid, resources=corr_ab)
                if dataFS is not None:
                    log.info(f'Creating new set of fitsteps for iov {iovid} and fitsteps list of size {len(dataFS)}')
                    out = self.cm.create_fitsteps(iovid=iovid, resources=dataFS)
                if dataFP is not None:
                    log.info(
                        f'Creating new set of fitpulls for iov {iovid} and fitpulls list of size {len(dataFP)}')
                    out = self.cm.create_fitpulls(iovid=iovid, resources=dataFP)
            elif cmd == 'imconfigs':
                log.info(f'Creating new IntervalMaker config with {tname} and args {pdic}')
                out = self.cm.create_im_conf(tagname=tname, **pdic)
            elif cmd == 'imtags':
                log.info(f'Creating new IntervalMaker tag with {tname} and args {pdic}')
                out = self.cm.create_im_tags(tagname=tname, **pdic)
            elif cmd == 'imiovs':
                since = None
                until = None
                mode = 'none'
                if 'since' in args:
                    st = args['since']
                    since = datetime.strptime(st, '%Y-%m-%d %H:%M:%SZ')
                if 'until' in args:
                    tt = args['until']
                    until = datetime.strptime(tt, '%Y-%m-%d %H:%M:%SZ')
                if 'mode' in args:
                    mode = args['mode']
                log.info(f'Creating new IM iovs for IM tag {tname} time {since} and {until}, mode is {mode}')
                out = self.cm.create_im_iovs(tagname=tname, since=since, until=until, mode=mode, fmt='iso')
                out['format'] = 'IntervalMakerIovSetDto'
                server_print(out, format=[])
            else:
                log.error(f'Command {cmd} is not recognized in this context')

        else:
            log.warning('Cannot create object without arguments')
        print(f'Response is : {out}')

    def do_convert(self, line):
        """convert date
        Convert a date to UTC unix time."""
        dt = datetime.fromisoformat(line)
        log.info('create time from string %s %s' % (line, dt.timestamp()))
        since = int(dt.timestamp() * 1000)
        print(f'date {line} = {since}')

    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            # Configuration

            # Remove this if you don't plan to "deactivate" the proxy later
            #        default_socket = socket.socket
            # Set up a proxy
            #            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

    def get_corrections_dict(self, corrs):
        pdic = {}
        pararr = corrs.split(',')
        for par in pararr:
            kv = par.split('=')
            pdic[kv[0]] = kv[1]
        return pdic

    def check_tagname(self, tag):
        if str(tag).startswith('EC_A'):
            return ('ENDCAP', 'SIDEA')
        elif str(tag).startswith('EC_C'):
            return ('ENDCAP', 'SIDEC')
        elif str(tag).startswith('BA_'):
            return ('BARREL', 'NONE')
        elif str(tag).startswith('TGC_A'):
            return ('TGC', 'SIDEA')
        elif str(tag).startswith('TGC_C'):
            return ('TGC', 'SIDEC')
        log.error('This tag name %s is not recognized as standard', tag)
        return None
