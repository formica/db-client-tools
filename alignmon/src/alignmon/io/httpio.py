"""
 HTTP client tool for Svom

 Retries requests with power law delays and a max tries limit

 @author: henri.louvin@cea.fr
"""

import os
import time
import urllib
import ssl
import requests
import asyncio
import aiohttp
import signal
# local imports
from .utils import get_ssl_context_dict, power_delay
from .tokens import KcTokens

# Log
import logging
log = logging.getLogger('http_client')
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

class HttpIo:
    """
    (A)synchronous HTTP client
    """
    def __init__(self, server_url='', max_tries=5, ssl_context=None, #pylint: disable=R0913
                 asynchronous=False,
                 loop=None,
                 use_tokens=False):
        self.server_url = server_url
        if server_url != '' and server_url[-1] != '/':
            self.server_url += '/'
        if not isinstance(max_tries, int) or max_tries == 0:
            raise ValueError('{max_tries} should be an integer greater than 0')
        self.max_tries = max_tries
        self.backoff_factor = 1
        self.loop = None
        self.asynchronous = asynchronous
        if asynchronous is True:
            # retrieve event loop
            try:
                self.loop = asyncio.get_event_loop() if loop is None else loop
            except RuntimeError:
                log.info('Failed to retrieve asyncio event loop. Creating a new one...')
                self.loop = asyncio.new_event_loop()

        self.kc_tokens = None
        if use_tokens:
            self.kc_tokens = KcTokens(
                backoff_factor=self.backoff_factor
            )
        self.ssl = {}
        if ssl_context is not None:
            print(f'ssl context not none: {ssl_context}')
            self.ssl = get_ssl_context_dict(ssl_context, self.asynchronous)
            print(f'ssl is now: {self.ssl}')


    def _get_session_headers(self):
        """ Builds authorization header from docker secret """
        # retrieve token secret name from env
        token_secret = os.environ.get('BASIC_TOKEN')
        if token_secret is None:
            log.warning('Empty environment variable BASIC_TOKEN,'
                        'falling back to default: basic_token')
            token_secret = 'basic_token'
        # retrieve token value from secret
        token = ''
        # return basic authorization header
        return {'Authorization': f'Basic {token}'}


    def _sync_request(self, req, url, **kwargs):
        """ Synchronous {req} request to {url} with data {kwargs}"""
        tried = 0
        response = None
        log.info('%s request to %s: %s', req, url, kwargs)
        with requests.Session() as session:
            if self.ssl != {}:
                print(f'The ssl env is not empty: {self.ssl}')
                session.cert = (self.ssl['cert'], self.ssl['key'])
                print(f'Session certificate: {session.cert}')

            if self.kc_tokens:
                # If use_tokens is True, automatically request Keycloak
                # for tokens, and format the Authorization header with
                # Keycloak response.
                auth_headers = self.kc_tokens.get_authorization_header()
                log.info(f'Use header : {auth_headers}')
                session.headers.update(auth_headers)

           # try until success or {self.max_tries} failures
            while tried < self.max_tries:
                tried += 1
                # catch SSLError to prevent exception propagation and sys exit
                try:
                    response = session.request(req, url, **kwargs)
                except (ssl.SSLError, requests.exceptions.SSLError) as exc:
                    response = requests.Response()
                    response.status_code = 496
                    response.json = lambda err=exc: {type(err).__name__: str(err)}
                    log.error('%s: %s', type(exc).__name__, str(exc))
                    return response
                except Exception as exc:
                    response = requests.Response()
                    response.status_code = 500
                    response.json = lambda err=exc: {type(err).__name__: str(err)}
                    log.error('%s: %s', type(exc).__name__, str(exc))
                    return response

                # retrieve response.json()
                json_resp = {}
                if response.headers.get('content-type') != 'application/octet-stream':
                    try:
                        json_resp = response.json()
                    except Exception:
                        pass
                log.debug('Server %s response (%s): %s', req, response.status_code, json_resp)
                # accept all responses with status 2xx
                if int(response.status_code/100) == 2:
                    log.info('%s request to %s successfull (%s)', req, url, response.status_code)
                    break
                if tried >= self.max_tries:
                    log.warning('%s request to %s failed (%s)',
                                req, url, response.status_code)
                    log.error('Last server %s response (%s): %s',
                              req, response.status_code, json_resp)
                    log.error('%s request to %s failed %s times. Aborting',
                              req, url, self.max_tries)
                    break
                delay = power_delay(tried, self.backoff_factor)
                log.warning('%s request to %s failed (%s). Trying again in %ss',
                            req, url, response.status_code, delay)
                time.sleep(delay)
                log.debug('%s request (%sth try) to %s: %s', req, tried+1, url, kwargs)
            # override response.json()
            if response is not None:
                response.json = lambda: json_resp
        return response

    async def _async_request(self, req, url, **kwargs):
        """ Asynchronous request to {url} with data {kwatgs}"""
        tried = 0
        response = None
        log.info('%s request (async) to %s: %s', req, url, kwargs)
        auth_headers = {}
        if self.kc_tokens:
            # If use_tokens is True, automatically request Keycloak
            # for tokens, and format the Authorization header with
            # Keycloak response.
            auth_headers = await self.kc_tokens.async_get_authorization_header()

        async with aiohttp.ClientSession(headers=auth_headers,
                                         connector=self.ssl.get('connector'),
                                         loop=self.loop) as session:
            # try until success or {self.max_tries} failures
            while tried < self.max_tries:
                tried += 1
                try:
                    # catch SSLError to prevent exception propagation and sys exit
                    try:
                        response = await session._request(req, url, **kwargs)
                    except (ssl.SSLError, requests.exceptions.SSLError) as exc:
                        response = requests.Response()
                        response.status_code = 496
                        response.json = lambda err=exc: {type(err).__name__: str(err)}
                        log.error('%s: %s', type(exc).__name__, str(exc))
                        return response
                    except Exception as exc:
                        response = requests.Response()
                        response.status_code = 500
                        response.json = lambda err=exc: {
                            type(err).__name__: str(err)}
                        log.error('%s: %s', type(exc).__name__, str(exc))
                        return response
                    # retrieve response.json()
                    json_resp = {}
                    try:
                        json_resp = await response.json()
                    except Exception:
                        pass
                    log.debug('Server %s response (%s): %s', req, response.status, json_resp)
                    # accept all responses with status 2xx
                    if int(response.status/100) == 2:
                        log.info('%s request (async) to %s successfull (%s)',
                                 req, url, response.status)
                        break
                    if tried >= self.max_tries:
                        log.warning('%s request (async) to %s failed (%s) for the %sth time.',
                                    req, url, response.status, tried)
                        log.error('Aborting after %sth request failure. Last server response: %s',
                                  tried, json_resp)
                        break
                    delay = power_delay(tried, self.backoff_factor)
                    log.warning('%s request (async) to %s failed (%s). Trying again in %ss',
                                req, url, response.status, delay)
                    await asyncio.sleep(delay)
                    log.debug('%s request (async, %sth try) to %s: %s',
                              req, tried+1, url, kwargs)
                except Exception as exc:
                    log.error('Exception caught: %s', exc)
                    if tried >= self.max_tries:
                        log.error('%s request (async) to %s failed %s times. Aborting',
                                  req, url, self.max_tries)
                        # re-raise exception if response is still None
                        # if response is None:
                        #     raise
                    delay = power_delay(tried, self.backoff_factor)
                    log.warning('%s request (async) to %s failed (Exception). Trying again in %ss',
                                req, url, delay)
                    await asyncio.sleep(delay)
                    log.debug('%s request (%sth async try) to %s: %s', req, tried+1, url, kwargs)
        # override response.json() and add status_code
        if response is not None:
            response.json = lambda: json_resp
            response.status_code = response.status
        return response

    def get(self, endpoint='/', **kwargs):
        """ GET request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return self._sync_request('GET', url, **kwargs)

    def post(self, endpoint='/', **kwargs):
        """ POST request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return self._sync_request('POST', url, **kwargs)

    def put(self, endpoint='/', **kwargs):
        """ PUT request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return self._sync_request('PUT', url, **kwargs)

    def delete(self, endpoint='/', **kwargs):
        """ DELETE request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return self._sync_request('DELETE', url, **kwargs)

    async def async_get(self, endpoint='/', **kwargs):
        """ GET request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return await self._async_request('GET', url, **kwargs)

    async def async_post(self, endpoint='/', **kwargs):
        """ POST request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return await self._async_request('POST', url, **kwargs)

    async def async_put(self, endpoint='/', **kwargs):
        """ PUT request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return await self._async_request('PUT', url, **kwargs)

    async def async_delete(self, endpoint='/', **kwargs):
        """ DELETE request to endpoint {endpoint} with json data {data}"""
        url = f"{self.server_url}{endpoint.lstrip('/')}"
        return await self._async_request('DELETE', url, **kwargs)

    def stop(self):
        """ close http session if asynchronous """
        if self.asynchronous is False:
            return
        log.debug('Stopping async client...')
        try:
            self.loop.stop()
            log.debug('Cancelling pending tasks...')
            # Cancel pending tasks
            pending = asyncio.all_tasks()
            for task in pending:
                task.cancel()
            self.loop.run_until_complete(asyncio.gather(*pending))
            # Stop async event loop
            log.debug('Done. Stopping async event loop')
        except Exception as err:
            log.error(err)

    def sigint_handler(self, signum, frame):
        """
        stops gracefully, restore default signal handling
        and raises KeyboardInterrupt
        """
        self.stop()
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        raise KeyboardInterrupt
