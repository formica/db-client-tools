"""
 HTTP client tool to exchange with VHF-PKT-DB

 Retries requests with power law delays and a max tries limit

 @author: henri.louvin@cea.fr
"""

# Log
import logging
# JSON
import json
from datetime import datetime, timezone

from .httpio import HttpIo

log = logging.getLogger('align_client')
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

print('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__, __name__, str(__package__)))


class AlignDbIo(HttpIo):
    """
    (A)synchronous HTTP client
    """

    def __init__(self, server_url=None, max_tries=5,  # pylint: disable=R0913
                 asynchronous=False, loop=None, use_tokens=True):
        if server_url is None:
            server_url = 'http://localhost:8080/api'
        print(f'Connection for server {server_url}')
        super().__init__(server_url,
                         max_tries=max_tries,
                         asynchronous=asynchronous,
                         loop=loop,
                         use_tokens=use_tokens)
        self.endpoints = {'opticallines': '/opticallines', 'opticallinesfind': '/opticallines/find',
                          'aligntags': '/aligntags',
                          'cooltags': '/cool/tags',
                          'imtags': '/intervals/tags',
                          'imiovs': '/intervals/iovs',
                          'imconfigs': '/intervals/configurations',
                          'aligniovs': '/aligniov',
                          'aligncorrections': '/aligniov/iovid/corrections',
                          'alignfitsteps': '/aligniov/iovid/fitsteps',
                          'alignfitpulls': '/aligniov/iovid/pulls',
                          'alignsagittas': '/aligniov/iovid/sagittas',
                          'oltestlines': '/opticallines/tests'}

        self.headers = {"Content-Type": "application/json", "Accept": "application/json"}
        self.align_headers = {}

    def set_header(self, hdr):
        """
        set {hdr} as self.align_headers
        """
        # example : {"X-Crest-PayloadFormat" : "JSON"}
        self.align_headers = hdr

    def create_opticallines(self, name=None, **kwargs):
        """
        import data into the database in json format
        usage example: create_opticallines(name='pra_BBB_YYY', oltype='PRA',
        olstatus='active')
        """
        # define output fields
        valid_fields = ['oltype', 'olstatus']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = {
            'olname': name,
            'oltype': 'JSON',
            'olstatus': 'time',
            'olformat': 'OpticallinesDto'}
        for key, val in kwargs.items():
            body_req[key] = val
        log.info('Create opticalline : %s', json.dumps(body_req))
        # send request
        resp = self.post(self.endpoints['opticallines'], json=body_req, headers=self.headers)
        return resp.json()

    def create_cooltags(self, name=None, aligntag=None, mode='create', **kwargs):
        """
        import data into the database in json format
        usage example: create_cooltags(name='MdtAlignECA-01', aligntag='SOME_ALIGN_TAG',
        tagType='TEST')
        """
        # define output fields
        valid_fields = ['tagComment', 'cherrypyConfig', 'coollock', 'doUploadOnReco', 'tagType']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = {
            'coolTag': name,
            'alignTag': aligntag,
            'tagComment': 'new cool tag association',
            'cherrypyConfig': 'SIMU',
            'coollock': 'unlocked',
            'doUploadOnReco': None,
            'tagType': 'TEST'
        }
        # send request
        if mode == 'create':
            for key, val in kwargs.items():
                body_req[key] = val
            log.debug('Create cooltag : %s', json.dumps(body_req))
            resp = self.post(self.endpoints['cooltags'], json=body_req, headers=self.headers)
        elif mode == 'update':
            body_req = {}
            for key, val in kwargs.items():
                body_req[key] = val
            resp = self.put(self.endpoints['cooltags'] + "/" + name, json=body_req, headers=self.headers)
        return resp.json()

    def create_tags(self, name=None, mode='create', **kwargs):
        """
        import data into the database in json format
        usage example: create_tags(name='EC_A_TEST', oltype='PRA',
        olstatus='active')
        """
        # define output fields
        valid_fields = ['tagDescription', 'tagInsertion', 'prevTag', 'tagAuthor', 'tagType', 'status']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        dnow = datetime.now(tz=timezone.utc)
        dnow = dnow.replace(microsecond=0)
        body_req = {
            'iovTag': name,
            'tagDescription': 'an align tag',
            'tagInsertion': dnow.isoformat(sep='T'),
            'prevTag': None,
            'tagAuthor': 'pyclient',
            'tagType': 'TESTS',
            'status': 'OPEN',
            'tagId': 0
        }
        # send request
        if mode == 'create':
            for key, val in kwargs.items():
                body_req[key] = val
            log.info('Create tag : %s', json.dumps(body_req))
            print('Create tag : %s', json.dumps(body_req))
            resp = self.post(self.endpoints['aligntags'], json=body_req, headers=self.headers)
        elif mode == 'update':
            body_req = {}
            for key, val in kwargs.items():
                body_req[key] = val
            resp = self.put(self.endpoints['aligntags'] + "/" + name, json=body_req, headers=self.headers)
        return resp.json()

    def create_iovs(self, name=None, since=None, until=None, **kwargs):
        """
        import data into the database in json format
        usage example: create_iovs(name='EC_A_TEST', since='2020-01-01 10:10:10+00:00',
        until='2020-01-01 10:10:10+00:00')
        """
        # define output fields
        valid_fields = ['coolStatus', 'fitStatus', 'iovComment']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        # sinceT = datetime.fromisoformat(since)
        # untilT = datetime.fromisoformat(until)
        # old:
        # sinceT.astimezone().isoformat(sep='T')
        # We assume the date parsing is done before, and here we have just the string.
        sinceT = since
        untilT = until
        body_req = {
            'sinceT': sinceT,
            'tillT': untilT,
            'coolStatus': 'NONE',
            'fitStatus': 0,
            'iovComment': 'none',
            'iovId': 0
        }
        for key, val in kwargs.items():
            body_req[key] = val
        log.info('Create iov : %s', json.dumps(body_req))
        # send request
        resp = self.post(self.endpoints['aligniovs'], params={'tagname': name}, json=body_req, headers=self.headers)
        return resp.json()

    def create_corrections(self, iovid=None, resources=[], **kwargs):
        """
        import data into the database in json format
        usage example: create_corrections(iovid=999, resources=[{},{},...])
        """
        # define output fields
        valid_fields = []

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = resources

        log.info('Create corrections of length : %s', len(resources))
        # send request
        purl = self.endpoints['aligncorrections'].replace('iovid', str(iovid))
        resp = self.post(purl, json=body_req, headers=self.headers)
        return resp.json()

    def create_fitsteps(self, iovid=None, resources=[], **kwargs):
        """
        import data into the database in json format
        usage example: create_fitsteps(iovid=999, resources=[{},{},...])
        """
        # define output fields
        valid_fields = []

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = resources

        log.info('Create corrections of length : %s', len(resources))
        # send request
        purl = self.endpoints['alignfitsteps'].replace('iovid', str(iovid))
        resp = self.post(purl, json=body_req, headers=self.headers)
        return resp.json()

    def create_fitpulls(self, iovid=None, resources=[], **kwargs):
        """
        import data into the database in json format
        usage example: create_fitpulls(iovid=999, resources=[{},{},...])
        """
        # define output fields
        valid_fields = []

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = resources

        log.info('Create fitpulls of length : %s', len(resources))
        # send request
        purl = self.endpoints['alignfitpulls'].replace('iovid', str(iovid))
        resp = self.post(purl, json=body_req, headers=self.headers)
        return resp.json()

    def create_sagittas(self, iovid=None, resources=[], **kwargs):
        """
        import data into the database in json format
        usage example: create_sagittas(iovid=999, resources=[{},{},...])
        """
        # define output fields
        valid_fields = []

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_req = resources

        log.info('Create sagittas of length : %s', len(resources))
        # send request
        purl = self.endpoints['alignsagittas'].replace('iovid', str(iovid))
        resp = self.post(purl, json=body_req, headers=self.headers)
        return resp.json()

    def create_im_conf(self, tagname=None, **kwargs):
        """
        import data into the database in json format
        it creates an intervalmaker configuration
        usage example: create_im_conf(tagname=FIXED_2H_TEST, tagDescription='a fixed 2H test tag', periodMinutes=120)
        """
        # define output fields
        valid_fields = ['tagDescription', 'periodMinutes', 'saveToDB', 'iovMinLength', 'iovRange', 'magCurrentCut']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        body_imconf_req = {
            'tag': tagname,
            'tagDescription': 'an intervalmaker configuration',
            'saveToDB': False
        }
        # Important: the other fields are relevant depending on IMTYPE (DynamicIntervalMakerConfDto, FixedIntervalMakerConfDto)
        # Dynamic: needs iovMinLength, iovRange, magCurrentCut
        # Fixed: needs periodMinutes
        for key, val in kwargs.items():
            body_imconf_req[key] = val
        if 'STABLE' in tagname:
            body_imconf_req['imtype'] = 'DynamicStableIovsIntervalMakerConfDto'
        elif 'FIXED' in tagname:
            body_imconf_req['imtype'] = 'FixedIntervalMakerConfDto'
        elif 'DYNAMIC' in tagname:
            body_imconf_req['imtype'] = 'DynamicIntervalMakerConfDto'
        else:
            log.error('Cannot create configuration because tag name is wrong...'
                      'should start with STABLE, DYNAMIC or FIXED')
            raise Exception('Wrong tag name provided')
        log.info('Create IntervalMaker configuration : %s', body_imconf_req)
        # send request
        resp = self.post(self.endpoints['imconfigs'], json=body_imconf_req, headers=self.headers)
        log.info(f'IntervalMakerConfiguration created : {resp}')
        return resp.json()

    def create_im_tags(self, tagname=None, **kwargs):
        """
        import data into the database in json format
        it creates an intervalmaker tag and related configuration
        usage example: create_im_tags(tagname=FIXED_2H_TEST, tagDescription='a fixed 2H test tag', periodMinutes=120)
        """
        # define output fields
        valid_fields = ['tagDescription']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)

        # prepare request arguments
        taginsertion = datetime.now(timezone.utc)
        print(
            f'Set datetime to {taginsertion} {taginsertion.strftime("%Y-%m-%dT%H:%M:%S%z")} {taginsertion.isoformat(sep="T")}')
        body_imtag_req = {
            'tagName': tagname,
            'tagDescription': 'an intervalmaker tag',
            'tagInsertion': None,
            'tagId': None
        }
        a = ['STABLE', 'FIXED', 'DYNAMIC']
        if not any(x in tagname for x in a):
            log.error('Cannot create a tag with a name not starting with STABLE, FIXED or DYNAMIC')
            raise Exception('Cannot create IM Tag with the specified tag name')
        for key, val in kwargs.items():
            body_imtag_req[key] = val

        log.info('Create IntervalMaker Tag : %s', body_imtag_req)
        # send request
        resp = self.post(self.endpoints['imtags'], json=body_imtag_req, headers=self.headers)
        return resp.json()

    def create_im_iovs(self, tagname=None, since=None, until=None, mode='none', fmt='ms'):
        """
        import data into the database in json format
        it creates intervalmaker iovs for a given IM tag using the strategy imposed by the configuration
        usage example: create_im_iovs(tagname=FIXED_2H_TEST, since=1000, until=2000)
        """
        if until < since:
            log.error(f'IntervalMakerIovs cannot be created for since {since} > until {until}')
            return 'Skip creation'
        # NB: the time arguments are in reality interpreted as strings. It is on server side we parse it.
        log.info(f'Create IntervalMaker iovs for range : {since} - {until} and tag={tagname} using format {fmt}')
        # send request
        criteria = {'since': since, 'till': until, 'mode': mode, 'tag': tagname}
        self.align_headers = {'X-DateFormat': fmt}

        resp = self.post(self.endpoints['imiovs'], params=criteria, headers=self.align_headers)
        log.info(f'IntervalMakerIovs created : {resp}')
        return resp.json()

    def search_tags(self, tagPattern=None, type=None):
        """
        request and export data from the database in json format
        usage example: search_tags(tagPattern='SVOM', type='PRODUCTION')
        """
        # send request
        criteria = {'tagPattern': tagPattern, 'type': type}
        resp = self.get(self.endpoints['aligntags'], params=criteria)
        return resp.json()

    def search_im_configurations(self, tagname='FIXED', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_im_configurations(tagname='DYNAMIC*')
        ?tag=DYNAMIC*”
        """
        # prepare request arguments
        criteria = {'tag': tagname}

        # send request
        resp = self.get(self.endpoints['imconfigs'], params=criteria)
        return resp.json()

    def search_im_tags(self, tagname='FIXED*', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_im_tags(tagname='DYNAMIC*')
        ?tag=DYNAMIC*”
        """
        # prepare request arguments
        criteria = {'tag': tagname}

        # send request
        resp = self.get(self.endpoints['imtags'], params=criteria)
        return resp.json()

    def search_im_iovs(self, mode='history', tagname='DEFAULT', since=0, until=1000, fmt='ms'):
        """
        request and export data from the database in json format
        usage example: search_im_iovs(tagname='DYNAMIC_V1',since=0,until=1000)
        ?tag=DYNAMIC_V1&since=0&till=1000”
        You can use as fmt either ms or an iso date [yyyyMMdd'T'HHmmssX]
        """
        # prepare request arguments
        criteria = {'tag': tagname, 'since': since, 'till': until, 'mode': mode}
        print(f'Search for IM IOVs using {criteria}')
        self.align_headers = {'X-DateFormat': fmt}
        # send request
        resp = self.get(self.endpoints['imiovs'], params=criteria, headers=self.align_headers)
        return resp.json()

    def search_cooltags(self, page=0, size=100, sort='pk.coolTag:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_tags(name='SVOM', author='AMELUNG')
        ?by=name:SVOM,author:AMELUNG”
        """
        # define output fields
        valid_filters = ['aligntag', 'cooltag', 'type']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)

        # prepare request arguments
        by_crit = ','.join([f'{key}:{val}' for key, val in kwargs.items()])
        criteria = {'by': by_crit, 'page': page, 'size': size, 'sort': sort}

        # send request
        resp = self.get(self.endpoints['cooltags'], params=criteria)
        return resp.json()

    def search_iovs(self, tagname=None, since=None, until=None, page=0, size=100, sort='iovId:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_iovs(tagname='SVOM', since='2020-01-01 10:00:00+00:00', until='2020-02-01 10:00:00+00:00')
        ?tagname=SVOM&since=xxx&until=yyy”
        """
        # define output fields
        valid_filters = ['fetchFitsteps', 'fetchBadsensors']
        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)
        fbs = False
        ffs = False
        for key, val in kwargs.items():
            print(f'checking arg {key} value {val}')
            if key == 'fetchFitsteps':
                ffs = val
            if key == 'fetchBadsensors':
                fbs = val
        # prepare request arguments
        criteria = {
            'tagname': tagname,
            'since': since,
            'until': until,
            'fetchBadsensors': fbs,
            'fetchFitsteps': ffs,
            'page': page, 'size': size, 'sort': sort
        }

        # send request
        resp = self.get(self.endpoints['aligniovs'], params=criteria)
        return resp.json()

    def delete_iovs(self, tagname=None, since=None, until=None, **kwargs):
        """
        request and export data from the database in json format
        usage example: delete_iovs(tagname='SVOM', since='2020-01-01 10:00:00+00:00', until='2020-02-01 10:00:00+00:00')
        ?tagname=SVOM&since=xxx&until=yyy”
        """
        # define output fields
        valid_filters = ['force']
        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)
        force = False
        # prepare request arguments
        criteria = {
            'tagname': tagname,
            'force': force
        }
        for key, val in kwargs.items():
            print(f'checking args {key} value {val}')
            criteria[key] = val
        if since is not None:
            criteria['since'] = since
        if until is not None:
            criteria['until'] = until
        # send request
        resp = self.delete(self.endpoints['aligniovs'], params=criteria)
        return resp.json()

    def get_iov_file(self, iovid=None, format='COOL'):
        """
        request and export data from the database in json format
        usage example: get_iov_file(iovid=1, format='COOL')
        """
        # define output fields

        # prepare request arguments
        url = self.endpoints['aligniovs']
        url = f'{url}/{iovid}/file'
        self.set_header({"X-IovFormat": "COOL"})
        # send request
        resp = self.get(url, headers=self.align_headers)
        if resp.status_code == 200:
            fout = f'/tmp/{iovid}.txt'
            # Write the file contents in the response to a file specified by local_file_path
            with open(fout, 'wb') as local_file:
                for chunk in resp.iter_content(chunk_size=128):
                    local_file.write(chunk)
            return fout
        return resp

    def search_corrections(self, page=0, size=100, sort='iovId:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_corrections(tagname='SVOM', since='>2020-01-01 10:00:00+00:00')
        ?by=tagname:SVOM,since>xxxx”
        """
        # define output fields
        valid_filters = ['since', 'until', 'element', 'iovId', 'tagname']

        # prepare request arguments
        for key, val in kwargs.items():
            print(f'checking arg {key} value {val}')
            if not val.startswith('>') and not val.startswith('<') and \
                    not val.startswith(':'):
                kwargs[key] = ':' + val
        by_crit = ','.join([f'{key}{val}' for key, val in kwargs.items()])
        criteria = {'by': by_crit, 'page': page, 'size': size, 'sort': sort}

        # send request
        resp = self.get(self.endpoints['aligncorrections'], params=criteria)
        return resp.json()

    def search_fitsteps(self, page=0, size=100, sort='iovId:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_fitsteps(tagname='SVOM', since='>2020-01-01 10:00:00+00:00')
        ?by=tagname:SVOM,since>xxxx”
        """
        # define output fields
        valid_filters = ['since', 'until', 'element', 'iovId', 'tagname']

        # prepare request arguments
        for key, val in kwargs.items():
            print(f'checking arg {key} value {val}')
            if not val.startswith('>') and not val.startswith('<') and \
                    not val.startswith(':'):
                kwargs[key] = ':' + val
        by_crit = ','.join([f'{key}{val}' for key, val in kwargs.items()])
        criteria = {'by': by_crit, 'page': page, 'size': size, 'sort': sort}

        # send request
        resp = self.get(self.endpoints['alignfitsteps'], params=criteria)
        return resp.json()

    def search_fitpulls(self, page=0, size=100, sort='pk.olname:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_fitpulls(tagname='SVOM', since='>2020-01-01 10:00:00+00:00')
        ?by=tagname:SVOM,since>xxxx”
        """
        # define output fields
        valid_filters = ['since', 'until', 'iovId', 'tagname']

        # prepare request arguments
        for key, val in kwargs.items():
            print(f'checking arg {key} value {val}')
            if not val.startswith('>') and not val.startswith('<') and \
                    not val.startswith(':'):
                kwargs[key] = ':' + val
        by_crit = ','.join([f'{key}{val}' for key, val in kwargs.items()])
        criteria = {'by': by_crit, 'page': page, 'size': size, 'sort': sort}

        # send request
        resp = self.get(self.endpoints['alignfitpulls'], params=criteria)
        return resp.json()

    def search_opticallines(self, page=0, size=100, sort='olname:ASC', **kwargs):
        """
        request and export data from the database in json format
        usage example: search_opticallines(olname='SVOM', oltype='BARREL')
        ?by=olname:SVOM,oltype:BARREL”
        """
        # define output fields
        valid_filters = ['olname', 'olstatus', 'oltype']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)

        # prepare request arguments
        by_crit = ','.join([f'{key}:{val}' for key, val in kwargs.items()])
        criteria = {'page': page, 'size': size, 'sort': sort}

        # send request
        resp = self.get(self.endpoints['opticallines'], params=criteria)
        return resp.json()

    def fetch_opticallines(self, **kwargs):
        """
        request and export data from the database in json format
        usage example: fetch_opticallines(name='pra_TEST', status='ACTIVE')
        ?name=pra_TEST&status=ACTIVE”
        """
        # define output fields
        valid_filters = ['name', 'status']

        # check request validity
        if not set(kwargs.keys()).issubset(valid_filters):
            log.error('Requested filters should be in %s', valid_filters)

        # prepare request arguments
        criteria = {'name': kwargs['name'], 'status': kwargs['status']}

        # send request
        resp = self.get(self.endpoints['opticallinesfind'], params=criteria)
        return resp.json()
