"""
 Keycloak Token tool.

 Request Keycloak server for user/app tokens, to set "Authorization" headers.

 @author: andrea.formica@cern.ch

-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
CLI: 'implicit credential flow, token exchange':
- client_id
- client_secret (not available)

To get an access token we must POST to keycloak with a payload like:
    payload = {
        "client_id": "ZeConfidentialClientId-ofTheCli",
        "grant_type": "password",
        "scope": "openid",
        "username: "the user name",
        "password": "the password"
    }
Retrieve the access token from that request, then send an exchange token request with:
    payload = {
        "client_id": "ZeConfidentialClientId-ofTheCli",
        "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange", (urlencode needed: --data-urlencode in curl)
        "subject_token": "the token from the previous request",
        "requested_token_type": "urn:ietf:params:oauth:token-type:refresh_token", (urlencode)
        "audience: "the target API ID"
    }
Retrieve the bearer token from that request and use it in the URLs of the API adding:
    header = {"Authorization": "Bearer XXX.YYY.ZZZ"}
"""
import json
import logging
import time
import os
import getpass

import requests
from .utils import _get_environ

log = logging.getLogger('tokens')
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

AUTH_TENANT = "auth.cern.ch/auth/realms/cern"
AUTH_TOKEN_ENDPOINT = "/protocol/openid-connect/token"
AUTH_URL = "https://{}{}".format(
    AUTH_TENANT,
    AUTH_TOKEN_ENDPOINT,
)
AUTH_HEADER = {"Content-Type": "application/x-www-form-urlencoded"}

SERVICE_KC_CLIENT_ID = 'align-mon-cli-client'
SERVICE_KC_TARGET_ID = 'atlas-muon-align-mon-server'


def _get_access_token_payload(username=None, password=None):
    """
    Prepare the access token request
    """
    client_id = _get_environ('CLIENT_ID', SERVICE_KC_CLIENT_ID)
    client_secret = None
    if username is None or password is None:
        # check if clientId/clientSecret are available from the environment
        client_secret = _get_environ('CLIENT_SECRET', None)
        if client_secret is None:
            # Ask for user/pass
            username = os.environ.get('API_USER')
            password = os.environ.get('API_PASSWORD')
        if username is None and not os.path.exists('.client-token.json'):
            print('User: ')
            username = input()
            password = getpass.getpass()
    if client_secret:
        return {
            "client_id": client_id,
            "client_secret": client_secret,
            "grant_type": "client_credentials"
        }
    return {
        "client_id": client_id,
        "grant_type": "password",
        "scope": "openid",
        "username" : username,
        "password": password
    }

def _get_app_token_payload(token=None, client_id=None, secret=None):
    """
    Prepare the bearer token request
    """
    if token is None or client_id is None:
        raise ValueError('Cannot prepare request for none token')
    if secret is None:
        return {
            "client_id": client_id,
            "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
            "subject_token": token,
            "requested_token_type": "urn:ietf:params:oauth:token-type:refresh_token",
            "audience": SERVICE_KC_TARGET_ID
        }
    else:
        return {
            "client_id": client_id,
            "client_secret": secret,
            "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
            "subject_token": token,
            "requested_token_type": "urn:ietf:params:oauth:token-type:refresh_token",
            "audience": SERVICE_KC_TARGET_ID
        }


class KcTokens:
    """A class to request users/app tokens from Keycloak auth server. An access token expires
    after a validity time. Meanwhile, no new requests are made to Keycloak.
    - if env var are not properly set, __init__ will raise ValueError.
    - if tokens requests fail -> format_header returns {},
        else {"Authorization": "Bearer XXX.YYY.ZZZ"}
    """

    def __init__(
            self,
            usr=None,
            pss=None,
            backoff_factor=1,
            expires_at_threshold=10
    ):
        """ The init method """
        self.payload = _get_access_token_payload(usr, pss)
        self.client_id = self.payload['client_id']
        self.tokens = None
        log.info(f' -- get token cli request payload: {self.payload}')
        self.payload_api = None
        self.tokens_api = None
        self.expires_at = None  # access token expiration time
        self.expires_at_threshold = expires_at_threshold  # latency
        self.backoff_factor = backoff_factor

    def store_payload(self, filename='.client-token.json', pyld=None):
        if os.path.exists(filename):
            log.info(f'File {filename} already exists...do not overwrite')
            return
        with open(filename, 'w') as f:
            json.dump(pyld, f)
        f.close()

    def read_payload(self, filename='.client-token.json'):
        p = None
        if not os.path.exists(filename):
            log.info(f'File {filename} does not exists...')
            return p
        with open(filename, 'r') as f:
            pyld = f.read()
            p = json.loads(pyld)
        f.close()
        return p

    def _request_exchange_tokens(self):
        """POST request to keycloak server for tokens."""
        log.info(" -- Auth: get exchange token")
        response = requests.post(
            AUTH_URL,
            data=self.payload,
            headers=AUTH_HEADER,
        )
        if response.status_code == 200:
            try:
                self.tokens = response.json()
                self.expires_at = (
                        time.time()
                        + self.tokens.get("expires_in", 0)
                        - self.expires_at_threshold
                )
            except Exception as err:
                log.error(" -- Auth: error to get json response: %s", err)
        else:
            log.error('Cannot get response from authentication server....')
        log.info(f'Retrieved tokens for exchange token request : {self.tokens}')

    def _request_api_tokens(self):
        log.info(" -- Auth: get api access token")

        token_type = self.tokens.get("token_type")
        access_token = self.tokens.get("access_token")
        log.info(f'Retrieved {token_type} : {access_token}')
        self.payload_api = _get_app_token_payload(access_token, client_id=self.client_id)

        log.info(" -- Auth: get api access token")
        response = requests.post(
            AUTH_URL,
            data=self.payload_api,
            headers=AUTH_HEADER,
        )
        if response.status_code == 200:
            try:
                self.tokens_api = response.json()
                self.expires_at = (
                        time.time()
                        + self.tokens_api.get("expires_in", 0)
                        - self.expires_at_threshold
                )
            except Exception as err:
                log.error(" -- Auth: error to get json response: %s", err)
        else:
            log.error('Cannot get response from authentication server....')
        log.info(f'Retrieved tokens for api request : {self.tokens_api}')

    def format_header(self):
        """ If tokens, format the Authorization header."""
        header = {}
        if self.tokens_api:
            token_type = self.tokens_api.get("token_type")
            access_token = self.tokens_api.get("access_token")
            header = {
                "Authorization": "{} {}".format(token_type, access_token),
            }
        else:
            log.warning("-- Auth: no Authorization header set.")
        return header

    def get_access_token(self):
        """ If tokens, retrieve type and token."""
        token_type = None
        access_token = None
        if self.tokens_api:
            token_type = self.tokens_api.get("token_type")
            access_token = self.tokens_api.get("access_token")
        else:
            log.warning("-- Auth: no Access Token available.")
        return token_type, access_token

    def get_authorization_header(self):
        """ Request for token if expired, and return the Authorization header. """
        if self.tokens and self.tokens_api:
            if time.time() > self.expires_at:
                self._request_exchange_tokens()
                self._request_api_tokens()

        if not self.tokens:
            self.tokens = self.read_payload('.client-token.json')
            log.info(f'reading back token from file: {self.tokens}')
            if self.tokens is None:
                self._request_exchange_tokens()

        if not self.tokens_api:
            self.tokens_api = self.read_payload('.client-api-token.json')
            log.info(f'reading back api token from file: {self.tokens_api}')
            if self.tokens_api is None:
                self._request_api_tokens()

        self.store_payload('.client-token.json', self.tokens)
        self.store_payload('.client-api-token.json', self.tokens_api)

        return self.format_header()
