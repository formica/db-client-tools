"""
 Utility functions to simplfy/uniformize NatsIO setup

 Henri Louvin - henri.louvin@cea.fr
"""

import argparse
# Logs
import logging
# System
import os
import sys

# NATS

log = logging.getLogger('utils_tools')

# UUID

# docker secrets

# ssl
import ssl
import aiohttp


def _get_environ(env_var, default_val):
    """
    Retrieves environment variable with fallback
    to {default_val} and logging
    """
    value = os.environ.get(env_var)
    if value is None:
        value = default_val
        msg = f"Empty environment variable {env_var}, " \
              f"falling back to default: {default_val}."
        log.warning(msg)
    return value

def _get_default_parser(**kwargs):
    parser = argparse.ArgumentParser(**kwargs)
    arggroup = parser.add_mutually_exclusive_group()
    arggroup.add_argument('-q', '--quiet', action='store_true',
                          help='Decrease verbosity')
    arggroup.add_argument('-v', '--verbose', action='store_true',
                          help='Increase verbosity')
    return parser


def _update_ssl_context(ssl_context):
    """
    Create or override {ssl_context} depending on its value and environment variables
    """
    if ssl_context == 'default' or 'SSL_DIR' in os.environ:
        # create or override all {ssl_context}
        home_dir = os.path.expanduser('~')
        ssl_dir = _get_environ('SSL_DIR', f'{home_dir}/.ssl')
        ssl_private_dir = _get_environ('SSL_PRIVATE_DIR', f'{ssl_dir}/private')
        ssl_certs_dir = _get_environ('SSL_CERTS_DIR', f'{ssl_dir}/certs')
        client_key = _get_environ('SSL_KEY_FILE',
                                  f'{ssl_private_dir}/align_client.key.pem')
        client_cert = _get_environ('SSL_CERT_FILE',
                                   f'{ssl_certs_dir}/align_client.cert.pem')
    elif len(ssl_context) == 2:
        client_key, client_cert = ssl_context
        if 'SSL_PRIVATE_DIR' in os.environ or 'SSL_KEY_FILE' in os.environ:
            # override key in {ssl_context}
            ssl_private_dir = os.environ.get('SSL_PRIVATE_DIR')
            client_key = _get_environ('SSL_KEY_FILE',
                                      f'{ssl_private_dir}/align_client.key.pem')
        if 'SSL_CERTS_DIR' in os.environ or 'SSL_CERT_FILE' in os.environ:
            # override cert in {ssl_context}
            ssl_certs_dir = os.environ.get('SSL_CERTS_DIR')
            client_cert = _get_environ('SSL_CERT_FILE',
                                       f'{ssl_certs_dir}/align_client.cert.pem')
    else:
        raise ValueError('ssl_context should be either "default" or a couple: '
                         ': (client_key, client_cert)')

    return client_key, client_cert


def get_ssl_context_dict(ssl_context, asynchronous=False):
    """
    Create SSL connector. {ssl_context} should be either 'default'
    or a 3-uple (client_key, client_cert)
    """
    try:
        # update {ssl_context}
        log.info('Creating SSL context...')
        client_key, client_cert = _update_ssl_context(ssl_context)

        # if one of the files is non-existent, disable SSL use
        for filename in client_key, client_cert:
            if not os.path.exists(filename):
                log.warning(f'Could not open file {filename}. DISABLING SSL')
                return {}

        # instantiate connector if asynchronous
        connector = None
        if asynchronous:
            ssl_ctx = ssl.create_default_context()
            ssl_ctx.load_cert_chain(client_cert, client_key)
            connector = aiohttp.TCPConnector(verify_ssl=True, ssl=ssl_ctx)
            log.info('SSL connector created: %s', connector)

    except Exception as exc:
        log.exception('SSL context creation failed')
        sys.exit(1)
    return {
        'key': client_key,
        'cert': client_cert,
        'connector': connector
    }

# delay computation function
def power_delay(try_num, backoff_factor=1):
    """ calculate delay in seconds as a power law """
    delay = backoff_factor * (2 ** (try_num - 1))
    return delay
