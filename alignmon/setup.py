"""
 distutils file to install the python Alignmon and Http communication wrapper httpio

 The resulting module is called natsio and can be used in python:
       * from alignmon.io import AlignDbIo

 Henri Louvin - henri.louvin@cea.fr
 Andrea Formica - andrea.formica@cern.ch
"""

from setuptools import setup

setup(
    name='align-mon',
    version='1',
    author="Henri Louvin, Andrea Formica",
    description="""Python module wrapping HttpIo for alignment monitoring use """,
    python_requires=">=3.5",
    packages=['alignmon.io', 'alignmon.cli', 'alignmon.utils'],
    package_dir={'alignmon.io': 'src/alignmon/io', 'alignmon.cli': 'src/alignmon/cli', 'alignmon.utils': 'src/alignmon/utils'},
    install_requires=['asyncio',
                      'requests==2.26.0',
                      'aiohttp==3.7.4',
                      'get_docker_secret==1.0.1'],
    zip_safe=False,
)
